

#ifndef		TREE_HH_
# define	TREE_HH_

#include <list>
#include <assert.h>
#include "node.hh"
// Rajouter l'include correspondant a la template de TYPENODE
// + implemente le std::cout abstrait pour les tests

template<class TYPENODE> class Tree
{
public:
  // Construit la racine de l'arbre avec l'info dans son contenu
  Tree(const TYPENODE& value) : _root(new Node<TYPENODE>(value)) {}

  Tree(const Tree<TYPENODE>& value) : _root(value._root) {}

  // Ajoute un noeud fils (avec info) a l'arbre
  void	add_node(const TYPENODE& value) { _root->add_child(value); }

  // Renvoie la racine de l'arbre de type Node
  Node<TYPENODE>&	get_node_root() const { return (*_root); }

  // Ajoute un autre arbre a la racine de l'arbre courant
  void	add_tree(const Tree &value)
  {
    _root->add_tree_node(value.get_node_root());
  }

  // Renvoie la liste de Node de ses fils
  std::list< Node<TYPENODE> >&	get_childs() { return _root->get_childs(); }

  // Information du nombre de fils
  const unsigned int	get_nb_childs() const
  {
    return (_root->get_nb_childs());
  }

  // Renvoie le noeud fils suivant le numero a partir de 0
  Node<TYPENODE>	get_child(const unsigned int& i) const
  {
    return (_root->get_child(i));
  }

private:
  Node<TYPENODE> *_root;
};

#endif	// TREE_HH_
