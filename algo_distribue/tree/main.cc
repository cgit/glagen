//
// main.cc for  in 
// 
// Made by meng-tih lam
// Login   <lam_m@epita.fr>
// 
// Started on  Fri Aug 16 02:41:45 2002 meng-tih lam
//

#include <iostream>
#include <list>
#include <string>

#include "node.hh"
#include "tree.hh"

// Pour construire un arbre, il faut d'abord faire les fils
// et les raccoller avec le pere avec la methode add_tree

int	test_tree()
{
  // ** Exemple avec la classe <string> **
  // ** On peut mettre n'importe classe **

  // * Support du sous-arbre *
  string root = "Connection...";
  string a = "Est-ce que ca fonctionne ?";
  string b = "1212123";
  string c = "OK recu 5/5";
  Tree<string> tree(root);
  tree.add_node(a);
  tree.add_node(b);
  tree.add_node(c);

  // Affichage root de l'arbre
  std::cout << "root: " << tree.get_node_root().get_data() << std::endl;

  // Affichage de ses fils
  unsigned int nb_childs = tree.get_nb_childs();
  for (unsigned int i = 0; i < nb_childs; ++i)
    std::cout << tree.get_child(i).get_data() << std::endl;
  std::cout << std::endl << std::endl;

  

  // * Support Arbre principale *
  Tree<string> main_tree("Execution...");

  // Voici la methode pour lier un arbre et un sous-arbre
  main_tree.add_tree(tree);

  std::cout << "root main: " << main_tree.get_node_root().get_data()
	    << std::endl;
  unsigned int nb_childs2 = main_tree.get_nb_childs();
  
  // Ici arbre sur 2 niveaux (Bien sur qu'on peut faire en recursivite ^_^)
  for (unsigned int i = 0; i < nb_childs2; ++i)
    {
      nb_childs = main_tree.get_child(i).get_nb_childs();
      std::cout << main_tree.get_child(i).get_data() << std::endl;
      for (unsigned int j = 0; j < nb_childs; ++j)
	std::cout << main_tree.get_child(i).get_child(j).get_data()
		  << std::endl;
    }
  std::cout << std::endl << std::endl;




  // * Allez on rajoute pour compliquer les choses *
  Tree<string> tree2("Une tentative de suicide");
  tree2.add_node("Ou ca?");
  tree2.add_node("A Epita");
  tree2.add_node("Ping pong ping");
  main_tree.add_tree(tree2);

  std::cout << "root main: " << main_tree.get_node_root().get_data()
	    << std::endl;
  nb_childs2 = main_tree.get_nb_childs();
  
  // Ici arbre sur 2 niveaux (Bien sur qu'on peut faire en recursivite ^_^)
  for (unsigned int i = 0; i < nb_childs2; ++i)
    {
      nb_childs = main_tree.get_child(i).get_nb_childs();
      std::cout << main_tree.get_child(i).get_data() << std::endl;
      for (unsigned int j = 0; j < nb_childs; ++j)
	std::cout << main_tree.get_child(i).get_child(j).get_data()
		  << std::endl;
    }

  return (0);
}


int	main()
{
  test_tree();
  return (0);
}
