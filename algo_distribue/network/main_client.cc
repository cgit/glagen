// Main du client

#include <unistd.h>
#include <assert.h>
#include "Client.hh"
#include "data/Data_exemple.hh"
#include "data/Data_string.hh"

void	help(char *prog_name)
{
  std::cout << prog_name << " [hostname] [port]" << std::endl;
  exit(0);
}

void	check_args(int argc, char **argv)
{
  int		current;

  if (argc != 3)
    {
      std::cerr << "Error client : Error parameters" << std::endl;
      help(argv[0]);
    }
  for (current = 0; argv[2][current] != '\0'; current++)
    if (argv[2][current] < '0' || argv[2][current] > '9')
      {
	std::cerr << "Error client : Error parameters" << std::endl;
	help(argv[0]);
      }
}

int	main(int argc, char **argv)
{
  check_args(argc, argv);
  Client client(argv[1], atoi(argv[2]));
//   Data<Data_exemple> data;
//   data.add_data(Data_exemple(10, 11, 13));
//   data.add_data(Data_exemple(1, 4, 81));
  client.wait_signal();
//   client.send_signal();
//   client.send_data(data);

//   Data<Data_string> data_string;
//   client.received_data(data_string);

  // On devrait implementer un numero a chaque classe associe
  // Je fais ca pour l'instant comme si on connait deja le type de classe
  // c'est juste pour tester
  Data<Data_string> data_string;
  client.received_data(data_string);

  while (0 == client.do_select());
  return (0);
}
