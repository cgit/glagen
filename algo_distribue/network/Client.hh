// Classe Client

#ifndef		CLIENT_HH_
# define	CLIENT_HH_

#include <list>
#include <iostream>
#include <string>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <assert.h>
#include <unistd.h>

#include "data/Data.hh"

class Client
{
public:
  Client(char* host, const int& port)
  {
    char	*c;
    int		sock;
    struct	sockaddr_in	sa;
    long	addr;
    struct	hostent	*host_info;
    
    if ((host_info = gethostbyname(host)) == 0)
      this->error();
    c = host_info->h_addr;
    addr = c[0] << 24 | c[1] << 16 | c[2] << 8 | c[3];
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
      this->error();
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(addr);
    if (connect(sock, (struct sockaddr *)&sa, sizeof (sa)) == -1)
      this->error();
    _fd_server = sock;
  }

  void	error()
  {
    std::cerr << "Error client : ";
    perror("");
    exit(errno);
  }

  template <class TYPEDATA>
  void	send_data(const Data<TYPEDATA>& data) const
  {
    data.send(_fd_server);
  }
  
  template <class TYPEDATA>
  void	received_data(Data<TYPEDATA>& data)
  {
    data.receive(_fd_server);
  }

  void	wait_signal()
  {
    unsigned char sig = 0;
    read(_fd_server, &sig, sizeof(unsigned char));
    if (sig != 42)
      {
	std::cout << "Erreur de transmission" << std::endl;
	exit(1);
      }
    std::cout << "En communication avec le serveur..." << std::endl;
  }

  void	send_signal()
  {
    unsigned char sig = 42;
    write(_fd_server, &sig, sizeof(unsigned char));
  }

  int	do_select()
  {
    fd_set	rd;
    char	buf[256];
    int		n;
    
    FD_ZERO(&rd);
    FD_SET(0, &rd);
    FD_SET(_fd_server, &rd);
    if (select(_fd_server + 1, &rd, 0, 0, 0) == -1)
      this->error();
    if (FD_ISSET(_fd_server, &rd))
      {
	n = read(_fd_server, buf, 150);
	if (n <= 0)
	  return (1);
	write(1, buf, n);
	fflush(0);
      }
    if (FD_ISSET(0, &rd))
      {
	n = read(0, buf, 100);
	write(_fd_server, buf, n);
      }		
    return (0);
  }
  
  
private:
  int	_fd_server;
};

#endif	// CLIENT_HH_
