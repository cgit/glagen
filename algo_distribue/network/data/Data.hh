// Classe Data

// Dans le TYPEDATA, il faut l'implementation de toString (retourne un string)
// et de type (retourne unsigned short int)

// Type 0: char
// Type 1: unsigned char
// Type 2: int
// Type 3: unsigned int
// Type 4: short int
// Type 5: unsigned short int
// Type 6: long int
// Type 7: unsigned long int
// Type 8: float
// Type 9: double
// Type 10: long double

#ifndef		DATA_HH_
# define	DATA_HH_

#include <list>
#include <iostream>
#include <stdio.h>

#include <errno.h>

template<class TYPEDATA> class Data
{
public:
  Data() : _data(new std::list<TYPEDATA>) {};

  Data(std::list<TYPEDATA>* data) : _data(data) {};

  void	error()
  {
    perror("");
    exit(errno);
  }

  void add_data(const TYPEDATA& data) { _data->push_back(data); };
  
  void send(const int& fd) const
  {
    std::list<TYPEDATA>::iterator data = _data->begin();
    unsigned int size = _data->size();
    std::cout << "Taille a ecrire:" << size << std::endl;
    write(fd, &size, sizeof(unsigned int));
    for (; data != _data->end(); ++data)
      data->write_data(fd);
  }

  void receive(const int& fd)
  {
    unsigned int size = 0;
    do
      {
	errno = 0;
	read(fd, &size, sizeof(unsigned int));
      }
    while(errno == 4);
    if (errno)
      this->error();
    std::cout << "Taille lu: " << size << std::endl;
    TYPEDATA data;
    for (unsigned int i = 0; i < size; ++i)
      {
	data.read_data(fd);
	_data->push_back(data);
      }
  }

  std::list<TYPEDATA>* get_data() { return (_data); }

private:
  std::list<TYPEDATA>*	_data;
};

#endif	// DATA_HH_
