// Classe Data_exemple

// Un exemple sur un point de coordonnees 3D

#ifndef		DATA_EXEMPLE_HH_
# define	DATA_EXEMPLE_HH_

#include <iostream>
#include <unistd.h> // Pour write/read

#include <errno.h>

class Data_exemple
{
public:
  Data_exemple() : _x(0), _y(0), _z(0) {};
  Data_exemple(int x, int y, int z) : _x(x), _y(y), _z(z) {};
  
  void	write_data(const int& fd) const
  {
    std::cout << "Donnees envoyes au serveur" << std::endl;
    std::cout << _x << std::endl;
    std::cout << _y << std::endl;
    std::cout << _z << std::endl;
    write(fd, &_x, sizeof(int));
    write(fd, &_y, sizeof(int));
    write(fd, &_z, sizeof(int));
  }
  
  void	read_data(const int& fd)
  {
    do
      {
	errno = 0;
	read(fd, &_x, sizeof(int));
      }
    while (errno == 4);

    do
      {
	errno = 0;
	read(fd, &_y, sizeof(int));
      }
    while (errno == 4);

    do
      {
	errno = 0;
	read(fd, &_z, sizeof(int));
      }
    while (errno == 4);

    std::cout << "Reception message sur le file descriptor :" << fd
	      << std::endl;
    std::cout << _x << std::endl;
    std::cout << _y << std::endl;
    std::cout << _z << std::endl;
  }

private:
  int _x;
  int _y;
  int _z;
};

#endif	// DATA_EXEMPLE
