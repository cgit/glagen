#ifndef		DATA_STRING_HH_
# define	DATA_STRING_HH_

#include <iostream>
#include <string>
#include <unistd.h> // Pour write/read

#include <errno.h>

class Data_string
{
public:
  Data_string() : _str("") {};
  Data_string(const string& str) : _str(str) {};
  
  void	write_data(const int& fd) const
  {
    
    std::cout << "Donnees envoyes" << std::endl << _str << std::endl;
    unsigned int size = _str.size();
    write(fd, &size, sizeof(unsigned int));
    for (unsigned int i = 0; i < size; ++i)
      {
	char car = _str[i];
	write(fd, &car, sizeof(char));
      }
  }
  
  void	read_data(const int& fd)
  {
    unsigned int size = 0;
    do
      {
	errno = 0;
	read(fd, &size, sizeof(size));
      }
    while (errno == 4);

    _str = "";
    for (unsigned int i = 0; i < size; ++i)
      {
	char car;
	do
	  {
	    errno = 0;
	    read(fd, &car, sizeof(char));
	  }
	while (errno == 4);
	_str += car;
      }

    std::cout << "Reception message sur le file descriptor :" << fd
	      << std::endl << _str << std::endl;
  }

private:
  string _str;
};

#endif	// DATA_STRING
