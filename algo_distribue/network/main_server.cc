// Main du reseau server

#include <assert.h>
#include "Server.hh"
#include "data/Data_exemple.hh"
#include "data/Data_string.hh"

char				gl_handle;

void	help(char *prog_name)
{
  std::cout << prog_name << " [port]" << std::endl;
  exit(0);
}

void	signal_alarm(int)
{
  gl_handle = gl_handle | 1;
}

void	check_args(int argc, char *argv1, char *argv2)
{
  int		current;

  if (argc != 2)
    {
      std::cerr << "Error server : Error parameters" << std::endl;
      help(argv1);
    }
  for (current = 0; argv2[current] != '\0'; current++)
    if (argv2[current] < '0' || argv2[current] > '9')
      {
	std::cerr << "Error server : Error parameters" << std::endl;
	help(argv1);
      }
}

int	main(int argc, char* argv[])
{   
//   int			fd_client;
//   socklen_t		len;
//   struct sockaddr_in	sock_client;
  
//   len = sizeof (struct sockaddr_in);
  check_args(argc, argv[0], argv[1]);
  Server server(atoi(argv[1]), 3);
  server.get_list_fd();
//   server.start_signal();
//   while (gl_handle != -1)
//     {
//       fd_client = 0;
//       if ((fd_client = accept(server.get_fd(),
// 			      (struct sockaddr *)&(sock_client), &len)) > 0)
// 	gl_handle = gl_handle | 2;
//       std::list<int> list_fd;
//       if (2 == (gl_handle & 2))
// 	{
// 	  server.accept_client(fd_client);
// 	  server.send_signal(fd_client);
// 	}
//       Data<Data_exemple> data;
//       server.wait_signal(list_fd);
//       server.received_data(data, list_fd);

//       Data<Data_string> data_string;
//       Data_string ack("Aknowledge");
//       data_string.add_data(ack);
//       server.send_data(data_string, list_fd);

//       gl_handle = 0;
//     }
//   return (0);
}
