// Classe Distribue



#ifndef		DISTRIBUE_HH_
# define	DISTRIBUE_HH_

#include "tree/tree.hh"
#include "tree/node.hh"
#include "network/data/Data.hh"
#include "network/Server.hh"
#include "tools/matrix.hh"

template<class TYPENODE> class Distribue
{
public:
  Distribue(const Tree<TYPENODE>& tree)
    : _tree(tree),
      _size(tree.get_node_root().get_number_node()),
      _mat(new Matrix<int> (_size, _size))
  {
    unsigned int ptr1 = 0, ptr2 = 0;
    std::list< Node<TYPENODE> > ptr_node = *new std::list< Node<TYPENODE> >;
    ptr_node.push_back(tree.get_node_root());
    while (ptr_node.size())
    {
      std::list< Node<TYPENODE> >::iterator childs = ptr_node.begin();
      std::list< Node<TYPENODE> >::iterator child =
	childs->get_childs()->begin();
      for (; child != childs->get_childs()->end(); ++child)
	{
	  ptr_node.push_back(*child);
	  (*_mat)(++ptr2, ptr1) = 1;
	}
      ptr_node.pop_front();
      ptr1++;
    }
  }

  void	depend(std::list<TYPENODE>& data, const unsigned int& ptr)
  {
    data.push_back(_tree.get_node_root().get_node_course_width(ptr).get_data());
    for (unsigned int i = 0; i < _size; ++i)
      if ((*_mat)(i, ptr))
	depend(data, i);
  }

  void	transfer(Server& server)
  {
    // Calcul du nombre de client necessaire
    unsigned int j = 0;
    std::list< list<TYPENODE> > list_data =
      *new std::list< list<TYPENODE> >;
    std::list<TYPENODE> data = *new std::list<TYPENODE>;
    for (unsigned int i  = 1; i < _size; ++i)
      {
	if ((*_mat)(i, 0)) // Ne prend que les noeuds fils du noeud root
	  {
	    ++j;
	    this->depend(data, i);
	    list_data.push_back(data);
	    data = *new std::list<TYPENODE>;
	  }
      }
    if (server.get_nb_client() < j)
      {
	std::cout << "Need more client" << std::endl;
	exit(-1);
      }

    std::list<int>::const_iterator fd_client =
      server.get_list_fd()->begin();
    data = *new std::list<TYPENODE>;
    for (unsigned int i  = 1; i < _size; ++i)
      {
	if ((*_mat)(i, 0)) // Ne prend que les noeuds fils du noeud root
	  {
	    ++j;
	    this->depend(data, i);
	    server.send_data(*fd_client++, Data<TYPENODE>(&data));
	    data = *new std::list<TYPENODE>;
	  }
      }
  }

  Matrix<int>&	get_matrix()
  {
    return (*_mat);
  }

private:
  Tree<TYPENODE> _tree;
  unsigned int _size;
  Matrix<int> *_mat;
};

#endif	// DISTRIBUTE_HH_
