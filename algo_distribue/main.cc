// Algo distribue

// glagen_divide [n]
// n indique le nombre d'ordinateur en reseau exclut le serveur

#include <iostream>
#include <list>
#include <string>

// Load Tree
#include "tree/node.hh"
#include "tree/tree.hh"

// Load Network
#include "network/Server.hh"
#include "network/Client.hh"

// Load Data
#include "network/data/Data_string.hh"

// Load Tool
#include "tools/matrix.hh"

#include "Distribue.hh"

#include <stdlib.h>
#include <assert.h>

// Prototype : test d'essais avec 2 clients
// On changera suivant les donnees passes par GTK
// Il faut que le nombre de client soit superieur ou egale
// au nombre de noeud fils par rapport au noeud principal
#define	NB_CLIENT 2

char gl_handle;

Tree<Data_string>	*create_tree();

void	help(char *prog_name)
{
  std::cout << prog_name << " [port]" << std::endl;
  exit(0);
}

void	signal_alarm(int)
{
  gl_handle = gl_handle | 1;
}

void	check_args(int argc, char *argv1, char *argv2)
{
  int		current;

  if (argc != 2)
    {
      std::cerr << "Error server : Error parameters" << std::endl;
      help(argv1);
    }
  for (current = 0; argv2[current] != '\0'; current++)
    if (argv2[current] < '0' || argv2[current] > '9')
      {
	std::cerr << "Error server : Error parameters" << std::endl;
	help(argv1);
      }
}

int	main(int argc, char *argv[])
{
  // Exemple de creation d'un arbre (donnees string)
  // On peut appele une classe abstraite dont ses fils sont
  // les types de donnees des librairies (ex: Tree<Abstraite*>)
  Tree<Data_string>* tree;
  tree = create_tree();

  // Calcul de l'algo distribue : calcul l'attribution des taches
  // pour chaque client
  Distribue<Data_string> network(*tree);
  std::cout << "Matrice calcule" << std::endl
	    << network.get_matrix() << std::endl;
  
  // Execution du serveur
  check_args(argc, argv[0], argv[1]);
  Server server(atoi(argv[1]), NB_CLIENT);

  // Transfert des donnees
  network.transfer(server);

  return (0);
}



Tree<Data_string>	*create_tree()
{
  string root = "Connection...";
  string a = "Est-ce que ca fonctionne ?";
  string b = "1212123";
  string c = "OK recu 5/5";
  Tree<Data_string> tree(root);
  tree.add_node(a);
  tree.add_node(b);
  tree.add_node(c);

  Tree<Data_string>* main_tree = new Tree<Data_string>(string("Execution..."));
  main_tree->add_tree(tree);

  Tree<Data_string> tree2(string("Une tentative de suicide"));
  tree2.add_node(string("Ou ca?"));
  tree2.add_node(string("A Epita"));
  tree2.add_node(string("Ping pong ping"));
  main_tree->add_tree(tree2);
  return (main_tree);
}
