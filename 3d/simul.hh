//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// simul.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		SIMUL_HH_
# define	SIMUL_HH_

#include	"dot.hh"

void		surface_solaire (Dot *, unsigned int);
void		atmosphere_solaire (Dot *, unsigned int);
void		essai_terrain (Dot *, unsigned int);
void		simule_lib_load ();


#endif		// SIMUL_HH_
