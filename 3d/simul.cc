//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// simul.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<cmath>
#include	"dot.hh"
#include	"data_glagen.hh"
#include	"perlin.hh"

//
// A garder : une librairie de simulation du Soleil
//
void		surface_solaire (Dot *m, unsigned int)
{
  property_t	*property;
  surface_t	*data;
  float		param[4];
  float		sum;

  if (m->Property (0) == NULL)
    {
      property = new property_t; // delete : dot.cc l59
      property->kind = surface;
      property->size = sizeof (surface_t);
      data = new surface_t; // delete : dot.cc l58
      property->data = data;
      m->Set_property (0, property);
    }
  else
    {
      property = m->Property (0);
      data = static_cast <surface_t *> (property->data);
    }

  param[0] = 10 * m->x () * 2;
  param[1] = 10 * m->y () * 2;
  param[2] = 10 * m->z () * 2;
  param[3] = glagen.float_time;
  sum = GLG_Perlin_noise_4D (param) / 40.0;
  param[0] = 20 * m->x () * 2;
  param[1] = 20 * m->y () * 2;
  param[2] = 20 * m->z () * 2;
  param[3] = glagen.float_time;
  sum = sum + GLG_Perlin_noise_4D (param) / 80.0;
  param[0] = 40 * m->x () * 2;
  param[1] = 40 * m->y () * 2;
  param[2] = 40 * m->z () * 2;
  param[3] = glagen.float_time;
  sum = sum + GLG_Perlin_noise_4D (param) / 160.0;
  data->height = 1.5 * sum;

  data->red = 1;
  data->green = (1 + 40 * data->height) / 2;
  data->blue = (data->height * data->height * 1600);
  data->alpha = (1 + 40 * data->height) / 2;
}

void		atmosphere_solaire (Dot *m, unsigned int)
{
  property_t	*property;
  surface_t	*data;
  float		param[4];
  float		sum;

  for (int layer = 0; layer < 100; layer++)
    {
      if (m->Property (layer + 1) == NULL)
	{
	  property = new property_t; // delete : dot.cc l59
	  property->kind = surface;
	  property->size = sizeof (surface_t);
	  data = new surface_t; // delete : dot.cc l58
	  property->data = data;
	  m->Set_property (layer + 1, property);
	}
      else
	{
	  property = static_cast <property_t *> (m->Property (layer + 1));
	  data = static_cast <surface_t *> (property->data);
	}
      param[0] = 3 * m->x () * 2;
      param[1] = 3 * m->y () * 2;
      param[2] = 3 * m->z () * 2;
      param[3] = (-(layer)/3.0 + glagen.float_time + 20) / 20.0;
      sum = GLG_Perlin_noise_4D (param);
      sum = sum * sum;
      sum = sum * sum;
      data->height = 0.01 + 0.006 * layer;
      data->red = 1;
      data->green = 0.8 - 7 * sum;
      data->blue = 0;
      data->alpha = 0.005 + (120 * sum) / (sqrtf((layer + 1.0) / 71) * 71);
    }
}

//
// Un essai de simulation de sol
//
void		essai_terrain (Dot *m, unsigned int level)
{
  property_t	*property;
  surface_t	*data;
  unsigned int	alpha;
  float		param[3];
  float		sum;

  if (m->Property (0) == NULL)
    {
      property = new property_t; // delete : dot.cc l59
      property->kind = surface;
      property->size = sizeof (surface_t);
      data = new surface_t; // delete : dot.cc l58
      property->data = data;
      m->Set_property (0, property);
    }
  else
    {
      property = static_cast <property_t *> (m->Property (0));
      data = static_cast <surface_t *> (property->data);
    }

  sum = 0;
  alpha = 1;
  param[0] = alpha * m->x () * 3;
  param[1] = alpha * m->y () * 3;
  param[2] = alpha * m->z () * 3;
  sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
  for (unsigned int current = 0; current < 2 * level; current++)
    {
      alpha = 2 * alpha;
      param[0] = alpha * m->x () * 3;
      param[1] = alpha * m->y () * 3;
      param[2] = alpha * m->z () * 3;
      sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
    }

  if (sum < 0)
    data->height = 0;
  else
    data->height = sum; // 10;

  if (data->height <= 0)
    {
      data->red = 0.0;
      data->green = 0.015 * (1 - sum);
      data->blue = 0.40 * (1 - sum);
    }
  else
    {
      if (data->height < 0.005)
	{
	  data->red = 0.52;
	  data->green = 0.46;
	  data->blue = 0.28;
	}
      else
	if (data->height > 0.04)
	    {
	      if (data->height > 0.0055)
		{
		  data->red = 0.9 + 4 * data->height;
		  data->green = 0.9 + 4 * data->height;
		  data->blue = 0.9 + 4 * data->height;
		}
	      else
		{
		  data->red = 0.55;
		  data->green = 0.41;
		  data->blue = 0.28;
		}
	    }
	else
	  {
	    data->red = 0.35;
	    data->green = 0.5;
	    data->blue = 0.21;
	  }
    }
  data->alpha = 1;
}

//
// Un autre essai de simulation de sol
//
void		essai_terrain_mono (Dot *m, unsigned int level)
{
  property_t	*property;
  surface_t	*data;
  unsigned int	alpha;
  float		param[3];
  float		sum;

  if (m->Property (0) == NULL)
    {
      property = new property_t; // delete : dot.cc l59
      property->kind = surface;
      property->size = sizeof (surface_t);
      data = new surface_t; // delete : dot.cc l58
      property->data = data;
      m->Set_property (0, property);
    }
  else
    {
      property = static_cast <property_t *> (m->Property (0));
      data = static_cast <surface_t *> (property->data);
    }

  sum = 0;
  alpha = 1;
  param[0] = alpha * m->x () * 3;
  param[1] = alpha * m->y () * 3;
  param[2] = alpha * m->z () * 3;
  sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
  for (unsigned int current = 0; current < 2 * level; current++)
    {
      alpha = 2 * alpha;
      param[0] = alpha * m->x () * 3;
      param[1] = alpha * m->y () * 3;
      param[2] = alpha * m->z () * 3;
      sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
    }

//   if (sum < 0)
//     data->height = 0;
//   else
    data->height = sum * 2;

  data->red = 1;
  data->green = 1;
  data->blue = 1;
  data->alpha = 1;
}

//
// La planete rouge
//
void		essai_mars (Dot *m, unsigned int level)
{
  property_t	*property;
  surface_t	*data;
  unsigned int	alpha;
  float		param[3];
  float		sum;

  if (m->Property (0) == NULL)
    {
      property = new property_t; // delete : dot.cc l59
      property->kind = surface;
      property->size = sizeof (surface_t);
      data = new surface_t; // delete : dot.cc l58
      property->data = data;
      m->Set_property (0, property);
    }
  else
    {
      property = static_cast <property_t *> (m->Property (0));
      data = static_cast <surface_t *> (property->data);
    }

  sum = 0;
  alpha = 1;
  param[0] = m->x () * 3;
  param[1] = m->y () * 3;
  param[2] = m->z () * 3;
  sum = sum + GLG_Perlin_noise_3D (param) / 10.0;
  for (unsigned int current = 0; current < 2 * level; current++)
    {
      alpha = 2 * alpha;
      param[0] = m->x () * 3 * alpha;
      param[1] = m->y () * 3 * alpha;
      param[2] = m->z () * 3 * alpha;
      sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
    }

  //
  //  - 0.2 < sum < 0.2
  //

  data->height = sum;

  if (data->height <= 0)
    {
//       data->red = 0.0;
//       data->green = 1.0;
//       data->blue = 1.0;
      data->red = 0.07 + 0.70 / (-sum);
      data->green = 0.07 + 0.20 / (-sum);
      data->blue = 0.05;
    }
  else
    {
      data->red = 0.77 + (1.05 * sum);
      data->green = 0.27 +  (1.25 * sum);
      data->blue = 0.05 +  (0.17 * sum);
    }
  data->alpha = 1;
}

//
// Un effet colore sympatique
//
void		essai_sympa (Dot *m, unsigned int level)
{
  property_t	*property;
  surface_t	*data;
  unsigned int	alpha;
  float		param[4];
  float		sum;
  int		sum_int;

  if (m->Property (0) == NULL)
    {
      property = new property_t; // delete : dot.cc l59
      property->kind = surface;
      property->size = sizeof (surface_t);
      data = new surface_t; // delete : dot.cc l58
      property->data = data;
      m->Set_property (0, property);
    }
  else
    {
      property = static_cast <property_t *> (m->Property (0));
      data = static_cast <surface_t *> (property->data);
    }

  sum = 0;
  alpha = 1;
  param[0] = alpha * m->x () * 3;
  param[1] = alpha * m->y () * 3;
  param[2] = alpha * m->z () * 3;
  param[3] = glagen.float_time;
  sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
  for (unsigned int current = 0; current < 2 * level; current++)
    {
      alpha = 2 * alpha;
      param[0] = alpha * m->x () * 3;
      param[1] = alpha * m->y () * 3;
      param[2] = alpha * m->z () * 3;
      param[3] = alpha * glagen.float_time;
      sum = sum + GLG_Perlin_noise_3D (param) / (10.0 * alpha);
    }
  data->height = 0;
  sum_int = (int)(1200 * ((10 * sum) + 0.5));
//   data->red = ((sum_int % 200) * 6) / 1200.0;
//   data->green = ((sum_int % 120) * 10) / 1200.0;
//   data->blue = ((sum_int % 100) * 12) / 1200.0;
  data->red = ((sum_int % 1200) * 1) / 1200.0;
  data->green = ((sum_int % 1200) * 1) / 1200.0;
  data->blue = ((sum_int % 600) * 2) / 1200.0;
  data->alpha = 1;
}

void		simule_lib_load ()
{
//  glagen.library[0].kind = none;

//   glagen.library[0].kind = surface;
//   glagen.library[0].dynamic = false;
//   glagen.library[0].run = essai_terrain;
//   glagen.library[0].name = "Terrain";

//   glagen.library[0].kind = surface;
//   glagen.library[0].dynamic = false;
//   glagen.library[0].run = essai_terrain_mono;
//   glagen.library[0].name = "Relief";

//  glagen.library[0].kind = surface;
//  glagen.library[0].dynamic = false;
//  glagen.library[0].run = essai_sympa;
//  glagen.library[0].name = "Couleurs";

 glagen.library[0].kind = surface;
 glagen.library[0].dynamic = true;
 glagen.library[0].run = surface_solaire;
 glagen.library[0].name = "surface_solaire";

//  glagen.library[1].kind = surface;
//  glagen.library[1].dynamic = true;
//  glagen.library[1].run = atmosphere_solaire;
//  glagen.library[1].name = "atmosphere_solaire";

  // Don't forget the end marker :
  glagen.library[1].kind = none;
}
