//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// frame.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	"data_glagen.hh"
#include	"frame.hh"
#include	"matrix3d.hh"
#include	"vector.hh"

// ================================================ Constructors and destructor

Frame :: Frame ()
{
  _origin[0] = 0;
  _origin[1] = 0;
  _origin[2] = 0;
  _basis[0] = Vector(1, 0, 0);
  _basis[1] = Vector(0, 1, 0);
  _basis[2] = Vector(0, 0, 1);
}

Frame :: Frame (float origin[3])
{
  _origin[0] = origin[0];
  _origin[1] = origin[1];
  _origin[2] = origin[2];
  _basis[0] = Vector(1, 0, 0);
  _basis[1] = Vector(0, 1, 0);
  _basis[2] = Vector(0, 0, 1);
}

Frame :: Frame (float origin[3], Vector basis[3])
{
  _origin[0] = origin[0];
  _origin[1] = origin[1];
  _origin[2] = origin[2];
  _basis[0] = Vector(basis[0]);
  _basis[1] = Vector(basis[1]);
  _basis[2] = Vector(basis[2]);
}

Frame :: ~Frame () {}

// ==================================================================== Reading

float		Frame :: Origin_X () const	{ return _origin[0]; }
float		Frame :: Origin_Y () const	{ return _origin[1]; }
float		Frame :: Origin_Z () const	{ return _origin[2]; }
Vector		Frame :: Basis_X () const	{ return _basis[0]; }
Vector		Frame :: Basis_Y () const	{ return _basis[1]; }
Vector		Frame :: Basis_Z () const	{ return _basis[2]; }

Vector		Frame :: View ()		{ return Vector (_basis[2]); }

Matrix44	Frame :: GetMatrix ()
{
  int		x;
  float		basis_matrix[3][3];

  for (x = 0; x < 3; x++)
    {
      basis_matrix[x][0] = _basis[x].X ();
      basis_matrix[x][1] = _basis[x].Y ();
      basis_matrix[x][2] = _basis[x].Z ();
    }
  Matrix44 rot_matrix(basis_matrix);
  Matrix44 trans_matrix(_origin);
  return Matrix44 (trans_matrix * rot_matrix);
}

Matrix44	Frame :: GetCamMatrix ()
{
  int		x;
  float		basis_matrix[3][3];

  for (x = 0; x < 3; x++)
    {
      basis_matrix[x][0] = _basis[x].X ();
      basis_matrix[x][1] = _basis[x].Y ();
      basis_matrix[x][2] = _basis[x].Z ();
    }
  Matrix44 rot_matrix(basis_matrix);
  Matrix44 trans_matrix(_origin);
  return Matrix44 (rot_matrix * trans_matrix);
}

// ==================================================================== Writing

void		Frame :: Reset_origin ()
{
  _origin[0] = 0;
  _origin[1] = 0;
  _origin[2] = 0;
}

void		Frame :: Reset_basis ()
{
  _basis[0].Set (1, 0, 0) ;
  _basis[1].Set (0, 1, 0) ;
  _basis[2].Set (0, 0, 1) ;
}

void		Frame :: Set_origin (const float origin[3])
{
  _origin[0] = origin[0];
  _origin[1] = origin[1];
  _origin[2] = origin[2];
}

void		Frame :: Translate (const Vector v)
{
  _origin[0] = _origin[0] + v.X ();
  _origin[1] = _origin[1] + v.Y ();
  _origin[2] = _origin[2] + v.Z ();
}

void		Frame :: Rotate (const float angle, const Vector v)
{
  Matrix33	rot (angle, v);

  _basis[0] = rot * _basis[0];
  _basis[1] = rot * _basis[1];
  _basis[2] = rot * _basis[2];
  _basis[0].Normalize ();
  _basis[1].Normalize ();
  _basis[2].Normalize ();
}
