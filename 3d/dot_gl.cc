//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// dot_gl.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		DARWIN
# include	<GL/gl.h>
# include	<GL/glu.h>
#else
# include	<gl.h>
# include	<glu.h>
#endif

#include	"data_glagen.hh"
#include	"dot.hh"

//
// Call the appropriate OpenGl functions for the dot
//
void		Dot :: Display (const unsigned int id)
{
  surface_t	*surface;
  float		adjust;

  if (glagen.display_planet)
    {
      if (NULL == _property[id])
	{
	  adjust = 0;
	  glColor3f ((1 + _x) / 2, (1 + _y) / 2, (1 + _z) / 2);
	}
      else
	{
	  surface = static_cast <surface_t *> (_property[id]->data);
	  if (NULL == surface)
	    {
	      adjust = 0;
	      glColor3f ((1 + _x) / 2, (1 + _y) / 2, (1 + _z) / 2);
	    }
	  else
	    {
	      adjust = surface->height;
	      glColor4f (surface->red,
			 surface->green,
			 surface->blue,
			 surface->alpha);
	    }
	}
    }
  else
    {
      adjust = 0;
      glColor3f ((1 + _x) / 2, (1 + _y) / 2, (1 + _z) / 2);
    }
  glNormal3f(_normal.X (), _normal.Y (), _normal.Z ());
  glVertex3f ((glagen.size + adjust) * _x,
	      (glagen.size + adjust) * _y,
	      (glagen.size + adjust) * _z);
}
