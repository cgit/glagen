//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// main.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	"data_glagen.hh"
#include	"triangle.hh"

void		destroy_tree (Triangle *triangle)
{
  Triangle	*next;
  bool		continue_deleting = true;

  while (continue_deleting)
    {
      if (0 == (next = triangle->Neighbor_ab ()))
	if (0 == (next = triangle->Neighbor_bc ()))
	  if (0 == (next = triangle->Neighbor_ca ()))
	    continue_deleting = false;
      delete triangle;
      triangle = next;
    }
  exit (0);
}

void		end_all ()
{
  destroy_tree(glagen.ref);
}
