//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// time_unix.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<sys/time.h>
#include	<cerrno>
#include        "data_glagen.hh"


void		init_time ()
{
  if (gettimeofday (&(glagen.time), NULL) != 0)
    exit (errno);
  glagen.start_time = glagen.time.tv_sec;
  glagen.time.tv_sec = 0;
  glagen.float_time = (glagen.time.tv_usec / 1000) / 1000.0;
}

void		update_time ()
{
  if (gettimeofday (&(glagen.time), NULL) != 0)
    exit (errno);
  glagen.time.tv_sec = glagen.time.tv_sec - glagen.start_time;
  glagen.float_time = (glagen.time.tv_sec +
		       (glagen.time.tv_usec / 1000) / 1000.0);
}
