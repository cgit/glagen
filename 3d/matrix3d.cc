//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// matrix.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<cmath>
#include	"data_glagen.hh"
#include	"matrix3d.hh"
#include	"vector.hh"

// =============================================== Constructors and destructors

Matrix33 :: Matrix33 ()
{
  for (int y = 0; y < 3; y++)
    for (int x = 0; x < 3; x++)
      mat[x][y] = (x == y ? 1 : 0);
}

Matrix33 :: Matrix33 (const class Matrix33 &source)
{
  for (int y = 0; y < 3; y++)
    for (int x = 0; x < 3; x++)
      mat[x][y] = source.A(x, y);
}

Matrix33 :: Matrix33 (const float m[3][3])
{
  for (int y = 0; y < 3; y++)
    {
      for (int x = 0; x < 3; x++)
	mat[x][y] = m[x][y];
      mat[3][y] = 0;
    }
  mat[0][3] = 0;
  mat[1][3] = 0;
  mat[2][3] = 0;
  mat[3][3] = 1;
}

Matrix33 :: Matrix33 (const float angle, const Vector &v)
{
  float c = cosf (angle);
  float s = sinf (angle);
  float t = 1 - c;
  float val[] = {t * v.X () * v.X () + c,
		 t * v.X () * v.Y () - s * v.Z (),
		 t * v.X () * v.Z () + s * v.Y (),

		 t * v.X () * v.Y () + s * v.Z (),
		 t * v.Y () * v.Y () + c,
		 t * v.Y () * v.Z () - s * v.X (),

		 t * v.X () * v.Z () - s * v.Y (),
		 t * v.Y () * v.Z () + s * v.X (),
		 t * v.Z () * v.Z () + c};
  for (int j = 0; j < 3; j++)
    for (int i = 0; i < 3; i++)
      mat[i][j] = val[3 * j + i];
}

Matrix33 :: ~Matrix33 () {}

Matrix44 :: Matrix44 ()
{
  for (int y = 0; y < 4; y++)
    for (int x = 0; x < 4; x++)
      mat[x][y] = (x == y ? 1 : 0);
}

Matrix44 :: Matrix44 (const class Matrix44 &source)
{
  for (int y = 0; y < 4; y++)
    for (int x = 0; x < 4; x++)
      mat[x][y] = source.A(x, y);
}

Matrix44 :: Matrix44 (const class Matrix33 &source)
{
  for (int y = 0; y < 3; y++)
    for (int x = 0; x < 3; x++)
      mat[x][y] = source.A(x, y);
  mat[3][3] = 1;
}

Matrix44 :: Matrix44 (const float m[4][4])
{
  for (int y = 0; y < 4; y++)
    for (int x = 0; x < 4; x++)
      mat[x][y] = m[x][y];
}

Matrix44 :: Matrix44 (const float m[3][3])
{
  for (int y = 0; y < 3; y++)
    {
      for (int x = 0; x < 3; x++)
	mat[x][y] = m[x][y];
      mat[3][y] = 0;
    }
  mat[0][3] = 0;
  mat[1][3] = 0;
  mat[2][3] = 0;
  mat[3][3] = 1;
}

// Create a translation matrix from a vector
Matrix44 :: Matrix44(const float colon[3])
{
  for (int y = 0; y < 4; y++)
    for (int x = 0; x < 4; x++)
      if (x == y)
	mat[x][y] = 1;
      else
	mat[x][y] = 0;
  mat[3][0] = colon[0];
  mat[3][1] = colon[1];
  mat[3][2] = colon[2];
}

Matrix44 :: ~Matrix44() {}


// ====================================================== The matrix's elements

float		Matrix33 :: A(const int x, const int y) const
{
  if (x >= 0 && x < 3 &&
      y >= 0 && y < 3)
    return mat[x][y];
  else
    return 0;
}

float		Matrix44 :: A(const int x, const int y) const
{
  if (x >= 0 && x < 4 &&
      y >= 0 && y < 4)
    return mat[x][y];
  else
    return 0;
}

// Orthonormalize
void		Matrix33 :: OrthonormalizeOrientation ()
{
  Vector	X (mat[0][0], mat[1][0], mat[2][0]);
  Vector	Y (mat[0][1], mat[1][1], mat[2][1]);
  Vector	Z;

  X.Normalize();
  Z = (X ^ Y);
  Z.Normalize();
  Y = (Z ^ X);
  Y.Normalize();

  mat[0][0] = X.X (); mat[0][1] = Y.X (); mat[0][2] = Z.X ();
  mat[1][0] = X.Y (); mat[1][1] = Y.Y (); mat[1][2] = Z.Y ();
  mat[2][0] = X.Z (); mat[2][1] = Y.Z (); mat[2][2] = Z.Z ();
}


// ================================================================== Operators

Vector		Matrix33 :: operator * (const Vector &v) const
{
  float		out[3];

  out[0] = (A (0, 0) * v.X () +
	    A (1, 0) * v.Y () +
	    A (2, 0) * v.Z ());
  out[1] = (A (0, 1) * v.X () +
	    A (1, 1) * v.Y () +
	    A (2, 1) * v.Z ());
  out[2] = (A (0, 2) * v.X () +
	    A (1, 2) * v.Y () +
	    A (2, 2) * v.Z ());
  return Vector (out[0], out[1], out[2]);
}

Matrix44	Matrix44 :: operator * (const Matrix44 &source) const
{
  float		out[4][4];

  for (int y = 0; y < 4; y++)
    for (int x = 0; x < 4; x++)
      out[x][y] = (A(0, y) * source.A(x, 0) +
		   A(1, y) * source.A(x, 1) +
		   A(2, y) * source.A(x, 2) +
		   A(3, y) * source.A(x, 3));
  return Matrix44(out);
}
