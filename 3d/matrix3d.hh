//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// matrix.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		MATRIX3D_HH_
# define	MATRIX3D_HH_

class		Matrix33
{
public :

  //Constructors and destructor
  Matrix33 ();
  Matrix33 (const class Matrix33 &);
  Matrix33 (const float m[3][3]);
  Matrix33 (const float, const class Vector &);
  ~Matrix33 ();

  // Matrix itself
  float		A (const int, const int) const;

  // Orthonormalize
  void		OrthonormalizeOrientation ();

  // Operator
  class Vector	operator * (const class Vector &) const;

protected :
  float		mat[3][3];
};

class		Matrix44
{
public :

  //Constructors and destructor
  Matrix44 ();
  Matrix44 (const class Matrix44 &);
  Matrix44 (const class Matrix33 &);
  Matrix44 (const float m[3][3]);
  Matrix44 (const float m[4][4]);
  Matrix44 (const float colon[3]);
  ~Matrix44 ();

  //Matrix itself
  float		A (const int, const int) const;

  //Operator
  Matrix44	operator * (const Matrix44 &) const;

protected :
  float		mat[4][4];
};

#endif		// MATRIX3D
