//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// vector.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<cmath>
#include	"data_glagen.hh"
#include	"misc.hh"
#include	"vector.hh"

// ================================================ Constructors and destructor

Vector :: Vector ()
{
  vect[0] = 0;
  vect[1] = 0;
  vect[2] = 0;
}

Vector :: Vector (const Vector &source)
{
  vect[0] = source.X ();
  vect[1] = source.Y ();
  vect[2] = source.Z ();
}

Vector :: Vector (const float x, const float y, const float z)
{
  vect[0] = x;
  vect[1] = y;
  vect[2] = z;
}

Vector :: ~Vector () {}


// ======================================================================= Read

float	Vector :: X () const		{ return vect[0]; }
float	Vector :: Y () const		{ return vect[1]; }
float	Vector :: Z () const		{ return vect[2]; }

float	Vector :: Norm () const
{
  return (sqrtf(vect[0] * vect[0] +
		vect[1] * vect[1] +
		vect[2] * vect[2]));
}

// Fast norm approximation computing
float Vector :: Approx_norm () const
{
  return (Approx_dist(vect[0], vect[1], vect[2]));
}


// ====================================================================== Write

void		Vector :: Set (const float x, const float y, const float z)
{
  vect[0] = x;
  vect[1] = y;
  vect[2] = z;
}

void		Vector :: Set_X (const float x){ vect[0] = x;}
void		Vector :: Set_Y (const float y){ vect[1] = y;}
void		Vector :: Set_Z (const float z){ vect[2] = z;}

void		Vector :: Normalize ()
{
  float norm = Norm ();
  vect[0] = vect[0] / norm;
  vect[1] = vect[1] / norm;
  vect[2] = vect[2] / norm;
}

void          Vector :: Approx_normalize ()
{
  float norm = Approx_norm ();
  vect[0] = vect[0] / norm;
  vect[1] = vect[1] / norm;
  vect[2] = vect[2] / norm;
}

void		Vector :: Rotate (const Vector &axis, const float angle)
{
  float cosinus = cosf (angle);
  float sinus = sinf (angle);
  float cos_bis = (1 - cosinus) * axis.X ();
  float cos_third = (1 - cosinus) * axis.Y ();

  float x = (cosinus + cos_bis * axis.X ()) * vect[0];
  x = x + (cos_bis * axis.Y () - axis.Z () * sinus) * vect[1];
  x = x + (cos_bis * axis.Z () + axis.Y () * sinus) * vect[2];

  float y = (cos_bis * axis.Y () + axis.Z () * sinus) * vect[0];
  y = y + (cosinus + cos_third * axis.Y ()) * vect[1];
  y = y + (cos_third * axis.Z () - axis.X () * sinus) * vect[2];

  float z = (cos_bis * axis.Z () - axis.Y () * sinus) * vect[0];
  z = z + (cos_third * axis.Z () + axis.X () * sinus) * vect[1];
  z = z + (cosinus + (1 - cosinus) * axis.Z () * axis.Z ()) * vect[2];

  vect[0] = x;
  vect[1] = y;
  vect[2] = z;
}


// ================================================================== Operators

//    @
//    @
//  @@@@@
//    @
//    @
Vector		Vector :: operator + (const class Vector &v) const
{
  return Vector(vect[0] + v.X (),
		vect[1] + v.Y (),
		vect[2] + v.Z ());
}

//
//
//  @@@@@
//
//
Vector		Vector :: operator - (const class Vector &v) const
{
  return Vector(vect[0] - v.X (),
		vect[1] - v.Y (),
		vect[2] - v.Z ());
}

//
//    @
//   @@@
//    @
//
float		Vector :: operator * (const class Vector &v) const
{
  return (vect[0] * v.X () +
	  vect[1] * v.Y () +
	  vect[2] * v.Z ());
}

//   @
//  @ @
// @   @
//
//
Vector		Vector :: operator ^ (const class Vector &v) const
{
  return Vector((vect[1] * v.Z ()) - (vect[2] * v.Y ()),
		(vect[2] * v.X ()) - (vect[0] * v.Z ()),
		(vect[0] * v.Y ()) - (vect[1] * v.X ()));
}

//   @
//   @   @@@@@
// @@@@@
//   @   @@@@@
//   @
Vector		Vector :: operator += (const class Vector &v)
{
  *this = *this + v;
  return *this;
}

//
//       @@@@@
// @@@@@
//       @@@@@
//
Vector		Vector :: operator -= (const class Vector &v)
{
  *this = *this - v;
  return *this;
}


//        @@@
//   @       @
//  @@@   @@@@
//   @   @   @
//        @@@ @
Vector		Vector :: operator * (const float &a) const
{
  return Vector(X () * a,
		Y () * a,
		Z () * a);
}

//     @  @@@
//    @      @
//   @    @@@@
//  @    @   @
// @      @@@ @
Vector		Vector :: operator / (const float &a) const
{
  return Vector(X () / a,
		Y () / a,
		Z () / a);
}

//              @@@
//   @   @@@@@     @
//  @@@         @@@@
//   @   @@@@@ @   @
//              @@@ @
Vector		Vector :: operator *= (const float &a)
{
  *this = *this * a;
  return *this;
}

//     @        @@@
//    @  @@@@@     @
//   @          @@@@
//  @    @@@@@ @   @
// @            @@@ @
Vector		Vector :: operator /= (const float &a)
{
  *this = *this / a;
  return *this;
}
