//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// library.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	"dot.hh"
#include	"triangle.hh"
#include	"data_glagen.hh"
#include	"simul.hh"

// This visitor applies the given library to each dot
void		library_visitor (void		(library)(Dot*, unsigned int),
				 bool		dynamic,
				 Triangle	*triangle)
{
  if (triangle->Is_checked ())
    return;

  if (triangle->A ()->Is_checked () == false &&
      (dynamic == true ||
       triangle->A ()->Is_computed () == false))
    {
      library (triangle->A (), triangle->Level ());
      triangle->A ()->Computed ();
      triangle->A ()->Checked ();
    }
  if (triangle->B ()->Is_checked () == false &&
      (dynamic == true ||
       triangle->B ()->Is_computed () == false))
    {
      library (triangle->B (), triangle->Level ());
      triangle->B ()->Computed ();
      triangle->B ()->Checked ();
    }
  if (triangle->C ()->Is_checked () == false &&
      (dynamic == true ||
       triangle->C ()->Is_computed () == false))
    {
      library (triangle->C (), triangle->Level ());
      triangle->C ()->Computed ();
      triangle->C ()->Checked ();
    }

  if (triangle->Is_split ())
    {
      library_visitor (library, dynamic, triangle->Child_center ());
      library_visitor (library, dynamic, triangle->Child_a ());
      library_visitor (library, dynamic, triangle->Child_b ());
      library_visitor (library, dynamic, triangle->Child_c ());
    }

  triangle->Checked ();
  if (triangle->Father() == NULL)
    {
      if (triangle->Neighbor_ab () != NULL)
	library_visitor (library, dynamic, triangle->Neighbor_ab ());
      if (triangle->Neighbor_bc () != NULL)
	library_visitor (library, dynamic, triangle->Neighbor_bc ());
      if (triangle->Neighbor_ca () != NULL)
	library_visitor (library, dynamic, triangle->Neighbor_ca ());
    }
}

// Here we call the visitor for each library
void		library_caller (Triangle *triangle)
{
  void		*(lib)(Dot *, unsigned int);

  for (int current = 0; glagen.library[current].kind != none; current++)
    {
      library_visitor (glagen.library[current].run,
		       glagen.library[current].dynamic,
		       triangle);
      glagen.step = glagen.step + 1;
    }
}
