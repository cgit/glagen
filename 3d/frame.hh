//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// frame.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		FRAME_HH_
# define	FRAME_HH_

#include	"matrix3d.hh"
#include	"vector.hh"

class		Frame
{
public:
  // Constructors and destructor
  Frame ();
  Frame (float origin[3]);
  Frame (float origin[3], class Vector basis[3]);
  ~Frame();

  // Reading
  float		Origin_X () const;
  float		Origin_Y () const;
  float		Origin_Z () const;
  class Vector	Basis_X () const;
  class Vector	Basis_Y () const;
  class Vector	Basis_Z () const;

  class Vector	View ();
  Matrix44	GetMatrix ();
  Matrix44	GetCamMatrix ();

  // Writing
  void		Reset_origin ();
  void		Reset_basis ();
  void		Set_origin (const float origin[3]);
  void		Translate (const class Vector);
  void		Rotate (const float, const class Vector);

protected:
  float		_origin[3];
  class Vector	_basis[3];
};

#endif		// FRAME_HH_
