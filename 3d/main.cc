//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// main.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<sys/time.h>
#include	<cerrno>
#include	<cmath>
#include	<cstdlib>
#include	"dot.hh"
#include	"data_glagen.hh"
#include	"display.hh"
#include	"frame.hh"
#include	"isosahedron.hh"
#include	"library.hh"
#include	"matrix3d.hh"
#include	"perlin.hh"
#include	"time.hh"
#include	"triangle.hh"
#include	"simul.hh"

glagen_t	glagen;

void		init_all ()
{
  float		origin[3];

  Perlin_init ();
  origin[0] = 0;
  origin[1] = 0;
  origin[2] = -3;
  glagen.observer.frame.Set_origin (origin);
  glagen.observer.altitude = sqrtf (glagen.observer.frame.Origin_X () *
				    glagen.observer.frame.Origin_X () +
				    glagen.observer.frame.Origin_Y () *
				    glagen.observer.frame.Origin_Y () +
				    glagen.observer.frame.Origin_Z () *
				    glagen.observer.frame.Origin_Z ()) - 1;
  glagen.observer.speed = 0.1 + glagen.observer.altitude / 50;
  glagen.triangles = 0;

  init_time ();

  //Limite : entre 50000 et 200000...
  glagen.max_triangles = 100000;
  glagen.step = 0;
  glagen.size = 1;
  glagen.square_size = glagen.size * glagen.size;
  //
  // Simulation
  //
  glagen.property = 200;
  // 0.05 et 0.4 is quite good
  glagen.split_dist_limit = 0.05;
  glagen.split_norm_limit = 0.4;
  //
  glagen.ref = isosahedron (glagen.size);
  glagen.step++;

  // Debug options
  glagen.display_normal = false;
  glagen.display_planet = true;
  glagen.display_all = false;
  glagen.third_view = false;
  glagen.zoom = true;
  glagen.depth_test = true;
  glagen.light = false;
}

int		main (int narg, char **args)
{
//  printf("int : %d\n", sizeof (int));
  init_all();
  simule_lib_load();

  if (glagen.zoom)
    {
      glagen.current = glagen.ref;
      glagen.ref->Split_visitor ();
      glagen.step = glagen.step + 1;
      glagen.ref->Hide_visitor ();
    }
  else
    {
      split_all (glagen.ref);
      glagen.step++;
      split_all (glagen.ref->Child_center ());
      glagen.step++;
      split_all (glagen.ref->Child_center ()->Child_center ());
      glagen.step++;
      split_all (glagen.ref->Child_center ()->Child_center ()->
		 Child_center ());
      glagen.step++;
      split_all (glagen.ref->Child_center ()->Child_center ()->
		 Child_center ()->Child_center ());
      glagen.step++;
      glagen.current = glagen.ref->
	Child_center ()->
	Child_center ()->
	Child_center ()->
	Child_center ()->
	Child_center ();
    }
  library_caller (glagen.ref);
  glagen.step++;
  display (&narg, &args);
  return 0;
}
