//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// triangle.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		TRIANGLE_HH_
# define	TRIANGLE_HH_

#include	<cstdlib>
#include	"data_glagen.hh"
#include	"dot.hh"
#include	"vector.hh"

class		Triangle
{
public:

  //Constructor and destructor
  Triangle (class Dot *, class Dot *, class Dot *, Triangle *);
  ~Triangle ();

  // Read
  unsigned int	Level () const;

  Dot		*A () const;
  Dot		*B () const;
  Dot		*C () const;

  Triangle	*Neighbor_ab () const;
  Triangle	*Neighbor_bc () const;
  Triangle	*Neighbor_ca () const;

  Triangle	*Father () const;

  Triangle	*Child_a() const;
  Triangle	*Child_b() const;
  Triangle	*Child_c() const;
  Triangle	*Child_center() const;
  Triangle	*Child (Dot *match) const;

  Vector	&Normal ();
  Vector	&Normal_real ();

  float		Size () const;
  bool		Is_split () const;
  bool		Is_checked () const;
  float		To_split () const;
  bool		Is_visible () const;

  // Write
  void		Make_connexity (Triangle *);
  void		Del_connexity (Triangle *);
  void		Split ();
  void		Check_T ();
  void		Merge ();
  void		Merge_simple ();
  void		Front_attack (Dot *, Dot *);
  void		Split_visitor ();
  bool		Hide_visitor ();
  void		Child_dead (Triangle *);
  void		Checked ();

  // Other
  void		Split_neighbor ();
  void		Update_normal ();
  void		Update_normal_visitor ();
  bool		T_case (const unsigned int);
  void		Display (const unsigned int);

protected:
  unsigned int	_level;

  Dot		*_a;
  Dot		*_b;
  Dot		*_c;

  Triangle	*_neighbor_ab;
  Triangle	*_neighbor_bc;
  Triangle	*_neighbor_ca;

  Triangle	*_father;

  Triangle	*_child_a;
  Triangle	*_child_b;
  Triangle	*_child_c;
  Triangle	*_child_center;

  bool		_visible;

  Vector	_center;
  Vector	_normal;
  Vector	_normal_real;
  float		_size;

  char		step;
};

#endif		// TRIANGLE_HH_
