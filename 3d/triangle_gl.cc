//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// triangle.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		DARWIN
# include	<GL/gl.h>
# include	<GL/glu.h>
#else
# include	<gl.h>
# include	<glu.h>
#endif

#include	"data_glagen.hh"
#include	"dot.hh"
#include	"triangle.hh"

//
// Manage the T-case (when the has a 4th point)
//
bool		Triangle :: T_case (const unsigned int id)
{
  Dot		*middle;
  Triangle	*child1;
  Triangle	*child2;

  // Is there a triangle on this side ?
  if (_neighbor_ab != NULL)

    // Then is it split ?
    if (_neighbor_ab->Is_split ())
      {
	// Find the dangling vertex
	child1 = _neighbor_ab->Child (_a);
	child2 = _neighbor_ab->Child (_b);
	if (child1->B () == child2->C ())
	  middle = child1->B ();
	else
	  middle = child1->C ();

	// Display two triangles instead of just one
	glBegin (GL_TRIANGLES);
	_b->Display (id);
	_c->Display (id);
	middle->Display (id);
	_c->Display (id);
	_a->Display (id);
	middle->Display (id);
	glEnd ();

	// The T case is fixed
	return true;
      }
  if (_neighbor_bc != NULL)
    if (_neighbor_bc->Is_split ())
      {
	child1 = _neighbor_bc->Child (_b);
	child2 = _neighbor_bc->Child (_c);
	if (child1->B () == child2->C ())
	  middle = child1->B ();
	else
	  middle = child1->C ();
	glBegin (GL_TRIANGLES);
	_a->Display (id);
	_b->Display (id);
	middle->Display (id);
	_c->Display (id);
	_a->Display (id);
	middle->Display (id);
	glEnd ();
	return true;
      }
  if (_neighbor_ca != NULL)
    if (_neighbor_ca->Is_split ())
      {
	child1 = _neighbor_ca->Child (_c);
	child2 = _neighbor_ca->Child (_a);
	if (child1->B () == child2->C ())
	  middle = child1->B ();
	else
	  middle = child1->C ();
	glBegin (GL_TRIANGLES);
	_b->Display (id);
	_c->Display (id);
	middle->Display (id);
	_a->Display (id);
	_b->Display (id);
	middle->Display (id);
	glEnd ();
	return true;
      }
  // In fact there's no T-case for this triangle
  return false;
}

//
// Display the faces included in the triangle (general case)
//
void		Triangle :: Display (const unsigned int id)
{
  // Test if this triangle has been visited yet
  if (step == glagen.step)
    return;
  step = glagen.step;

  if (glagen.display_all == true || _visible == true)
    {
      // If the triangle has childs, we visit them instead
      if (_child_center != NULL)
	{
	  _child_a->Display (id);
	  _child_b->Display (id);
	  _child_c->Display (id);
	  _child_center->Display (id);
	}
      else
	{
	  // T-case managment
	  if (false == T_case (id))
	    {
	      if (glagen.display_normal == true)
		{
		  float x = (_a->x () + _b->x () + _c->x ()) / 3;
		  float y = (_a->y () + _b->y () + _c->y ()) / 3;
		  float z = (_a->z () + _b->z () + _c->z ()) / 3;

		  // Display the normal vector
		  if (glagen.light)
		    glDisable(GL_LIGHTING);
		  glBegin (GL_LINES);
		  glColor3f(1.0, 0.2, 0.0);
		  glVertex3f(x, y, z);
		  glVertex3f(x + _normal_real.X () / 100,
			     y + _normal_real.Y () / 100,
			     z + _normal_real.Z () / 100);
		  glEnd ();
		  if (glagen.light)
		    glEnable(GL_LIGHTING);
		}
	      // Could it be more simple ?
	      glBegin (GL_TRIANGLES);
	      _a->Display (id);
	      _b->Display (id);
	      _c->Display (id);
	      glEnd ();
	    }
	}
    }
  // Recursive call at the top of the three
  if (NULL == Father ())
    {
      _neighbor_ab->Display (id);
      _neighbor_bc->Display (id);
      _neighbor_ca->Display (id);
    }
}
