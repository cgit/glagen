//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// perlin.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

// Original implementation fo Perlin noice.
// It can be found on :
// http://mrl.nyu.edu/~perlin/doc/oscar.html#noise
//
// The 4th dimension noise has been added for our needs

// coherent noise function over 1, 2 or 3 dimensions
// (copyright Ken Perlin)

#include <stdlib.h>
#include <math.h>
#include "data_glagen.hh"

#define	B 0x100
#define	BM 0xff

#define	N 0x1000
#define	NP 12   /* 2^N */
#define	NM 0xfff

static int	p[B + B + 2];
static float	g4[B + B + 2][4];
static float	g3[B + B + 2][3];
static float	g2[B + B + 2][2];
static float	g1[B + B + 2];

#define s_curve(t) ( t * t * (3. - 2. * t) )

#define lerp(t, a, b) ( a + t * (b - a) )

#define setup(i,b0,b1,r0,r1)\
	t = vec[i] + N;\
	b0 = ((int)t) & BM;\
	b1 = (b0+1) & BM;\
	r0 = t - (int)t;\
	r1 = r0 - 1.;

float		GLG_Perlin_noise_1D (float arg)
{
  int		bx0, bx1;
  float	rx0, rx1;
  float	sx;
  float	t, u, v;
  float	vec[1];

  vec[0] = arg;

  setup(0, bx0,bx1, rx0,rx1);

  sx = s_curve(rx0);

  u = rx0 * g1[ p[ bx0 ] ];
  v = rx1 * g1[ p[ bx1 ] ];

  return lerp(sx, u, v);
}

float		GLG_Perlin_noise_2D (float vec[2])
{
  int		bx0, bx1;
  int		by0, by1;
  int		b00, b10, b01, b11;
  float		rx0, rx1;
  float		ry0, ry1;
  float		*q, sx, sy;
  float		a, b;
  float		t, u, v;
  register int	i, j;

  setup(0, bx0,bx1, rx0,rx1);
  setup(1, by0,by1, ry0,ry1);

  i = p[ bx0 ];
  j = p[ bx1 ];

  b00 = p[ i + by0 ];
  b10 = p[ j + by0 ];
  b01 = p[ i + by1 ];
  b11 = p[ j + by1 ];

  sx = s_curve(rx0);
  sy = s_curve(ry0);

#define at2(rx,ry) ( rx * q[0] + ry * q[1] )

  q = g2[ b00 ] ; u = at2(rx0,ry0);
  q = g2[ b10 ] ; v = at2(rx1,ry0);
  a = lerp(sx, u, v);

  q = g2[ b01 ] ; u = at2(rx0,ry1);
  q = g2[ b11 ] ; v = at2(rx1,ry1);
  b = lerp(sx, u, v);

  return lerp(sy, a, b);
}

float		GLG_Perlin_noise_3D (float vec[3])
{
  int		bx0, bx1;
  int		by0, by1;
  int		bz0, bz1;
  int		b00, b10, b01, b11;
  float		rx0, rx1;
  float		ry0, ry1;
  float		rz0, rz1;
  float		*q, sy, sz;
  float		a, b, c, d;
  float		t, u, v;
  register int	i, j;

  setup(0, bx0,bx1, rx0,rx1);
  setup(1, by0,by1, ry0,ry1);
  setup(2, bz0,bz1, rz0,rz1);

  i = p[ bx0 ];
  j = p[ bx1 ];

  b00 = p[ i + by0 ];
  b10 = p[ j + by0 ];
  b01 = p[ i + by1 ];
  b11 = p[ j + by1 ];

  t  = s_curve(rx0);
  sy = s_curve(ry0);
  sz = s_curve(rz0);

#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )

  q = g3[ b00 + bz0 ] ; u = at3(rx0,ry0,rz0);
  q = g3[ b10 + bz0 ] ; v = at3(rx1,ry0,rz0);
  a = lerp(t, u, v);

  q = g3[ b01 + bz0 ] ; u = at3(rx0,ry1,rz0);
  q = g3[ b11 + bz0 ] ; v = at3(rx1,ry1,rz0);
  b = lerp(t, u, v);

  c = lerp(sy, a, b);

  q = g3[ b00 + bz1 ] ; u = at3(rx0,ry0,rz1);
  q = g3[ b10 + bz1 ] ; v = at3(rx1,ry0,rz1);
  a = lerp(t, u, v);

  q = g3[ b01 + bz1 ] ; u = at3(rx0,ry1,rz1);
  q = g3[ b11 + bz1 ] ; v = at3(rx1,ry1,rz1);
  b = lerp(t, u, v);

  d = lerp(sy, a, b);

  return lerp(sz, c, d);
}

float		GLG_Perlin_noise_4D (float vec[4])
{
  int		bx0, bx1;
  int		by0, by1;
  int		bz0, bz1;
  int		bt0, bt1;
  register int	a0, a1;
  int		b00, b10, b01, b11;
  int		c000, c001, c010, c011, c100, c101, c110, c111;
  float		rx0, rx1;
  float		ry0, ry1;
  float		rz0, rz1;
  float		rt0, rt1;
  float		*q, sx, sy, sz, st;
  float		a, b, c, d, e, f;
  float		t, u, v;

  setup(0, bx0,bx1, rx0,rx1);
  setup(1, by0,by1, ry0,ry1);
  setup(2, bz0,bz1, rz0,rz1);
  setup(3, bt0,bt1, rt0,rt1);

  a0 = p[ bx0 ];
  a1 = p[ bx1 ];

  b00 = p[ a0 + by0 ];
  b10 = p[ a1 + by0 ];
  b01 = p[ a0 + by1 ];
  b11 = p[ a1 + by1 ];

  c000 = p[ b00 + bz0 ];
  c100 = p[ b10 + bz0 ];
  c010 = p[ b01 + bz0 ];
  c110 = p[ b11 + bz0 ];
  c001 = p[ b00 + bz1 ];
  c101 = p[ b10 + bz1 ];
  c011 = p[ b01 + bz1 ];
  c111 = p[ b11 + bz1 ];

  sx = s_curve(rx0);
  sy = s_curve(ry0);
  sz = s_curve(rz0);
  st = s_curve(rt0);

#define at4(rx, ry, rz, rt) ( rx * q[0] + ry * q[1] + rz * q[2] + rt * q[3])

  q = g4[c000 + bt0]; u = at4(rx0, ry0, rz0, rt0);
  q = g4[c100 + bt0]; v = at4(rx1, ry0, rz0, rt0);
  a = lerp(sx, u, v);

  q = g4[c010 + bt0]; u = at4(rx0, ry1, rz0, rt0);
  q = g4[c110 + bt0]; v = at4(rx1, ry1, rz0, rt0);
  b = lerp(sx, u, v);

  c = lerp (sy, a, b);

  q = g4[c001 + bt0]; u = at4(rx0, ry0, rz1, rt0);
  q = g4[c101 + bt0]; v = at4(rx1, ry0, rz1, rt0);
  a = lerp(sx, u, v);

  q = g4[c011 + bt0]; u = at4(rx0, ry1, rz1, rt0);
  q = g4[c111 + bt0]; v = at4(rx1, ry1, rz1, rt0);
  b = lerp(sx, u, v);

  d = lerp (sy, a, b);


  e = lerp (sz, c, d);


  q = g4[c000 + bt1]; u = at4(rx0, ry0, rz0, rt1);
  q = g4[c100 + bt1]; v = at4(rx1, ry0, rz0, rt1);
  a = lerp(sx, u, v);

  q = g4[c010 + bt1]; u = at4(rx0, ry1, rz0, rt1);
  q = g4[c110 + bt1]; v = at4(rx1, ry1, rz0, rt1);
  b = lerp(sx, u, v);

  c = lerp (sy, a, b);

  q = g4[c001 + bt1]; u = at4(rx0, ry0, rz1, rt1);
  q = g4[c101 + bt1]; v = at4(rx1, ry0, rz1, rt1);
  a = lerp(sx, u, v);

  q = g4[c011 + bt1]; u = at4(rx0, ry1, rz1, rt1);
  q = g4[c111 + bt1]; v = at4(rx1, ry1, rz1, rt1);
  b = lerp(sx, u, v);

  d = lerp (sy, a, b);


  f = lerp (sz, c, d);


  return lerp (st, e, f);
}

static void	normalize2 (float v[2])
{
  float		s;

  s = sqrtf(v[0] * v[0] + v[1] * v[1]);
  v[0] = v[0] / s;
  v[1] = v[1] / s;
}

static void	normalize3 (float v[3])
{
  float		s;

  s = sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
  v[0] = v[0] / s;
  v[1] = v[1] / s;
  v[2] = v[2] / s;
}

static void	normalize4 (float v[4])
{
  float		s;

  s = sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3]);
  v[0] = v[0] / s;
  v[1] = v[1] / s;
  v[2] = v[2] / s;
  v[3] = v[3] / s;
}

void		Perlin_init ()
{
  int		i, j, k;

  for (i = 0 ; i < B ; i++)
    {
      p[i] = i;

      g1[i] = (float)((rand() % (B + B)) - B) / B;

      for (j = 0 ; j < 2 ; j++)
	g2[i][j] = (float)((rand() % (B + B)) - B) / B;
      normalize2(g2[i]);

      for (j = 0 ; j < 3 ; j++)
	g3[i][j] = (float)((rand() % (B + B)) - B) / B;
      normalize3(g3[i]);

      for (j = 0 ; j < 4 ; j++)
	g4[i][j] = (float)((rand() % (B + B)) - B) / B;
      normalize4(g4[i]);
    }

  while (--i)
    {
      k = p[i];
      p[i] = p[j = rand() % B];
      p[j] = k;
    }

  for (i = 0 ; i < B + 2 ; i++)
    {
      p[B + i] = p[i];
      g1[B + i] = g1[i];
      for (j = 0 ; j < 2 ; j++)
	g2[B + i][j] = g2[i][j];
      for (j = 0 ; j < 3 ; j++)
	g3[B + i][j] = g3[i][j];
    }
}
