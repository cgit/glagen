//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// data_glagen.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		DATA_GLAGEN_HH_
# define	DATA_GLAGEN_HH_

#ifdef		DARWIN
# define	float	double
# define	sqrtf	sqrt
# define	sinf	sin
# define	cosf	cos
#endif

#include	<sys/time.h>
#include	<cstdlib>
#include	"frame.hh"

typedef			struct
{
  class Frame		frame;
  float			speed;
  float			altitude;
}			observer_t;

typedef			enum
  {
    none,
    surface,
    object,
    non_material,
    other
  }			kind_lib_t;

typedef			struct
{
  kind_lib_t		kind;
  size_t		size;
  void			*data;
}			property_t;

typedef			struct
{
  kind_lib_t		kind;
  bool			dynamic;
  void			(*run)(class Dot *, unsigned int);
  char			*name;
}			library_t;

typedef			struct
{
  observer_t		observer;
  struct timeval	time;
  long			start_time;
  float			float_time;
  char			step;
  float			size;
  float			square_size;
  class Triangle	*ref;
  class Triangle	*current;
  int			property;
  library_t		library[5];
  float			split_dist_limit;
  float			split_norm_limit;
  unsigned int		triangles;
  unsigned int		max_triangles;
  bool			display_normal;
  bool			display_planet;
  bool			display_all;
  bool			third_view;
  bool			zoom;
  bool			light;
  int			mode;
  int			depth_test;
  int			x;
  int			y;
}			glagen_t;

extern glagen_t		glagen;

typedef			struct
{
  float			height;
  float			red;
  float			green;
  float			blue;
  float			alpha;
}			surface_t;

#endif		// DATA_GLAGEN_HH_
