//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// isosahedron.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<cmath>
#include	"data_glagen.hh"
#include	"dot.hh"
#include	"triangle.hh"

// The regular polyhedron we use to create the planet is an isosahedron :
// 20 triangular faces, 12 vertex and 30 edges
//
// Vertex coordinates :
//
// {  0,			1,		 0			} = P0
// {  sqrtf((5 - sqrtf(5)) / 10), (sqrtf(5)) / 5, -(5 + sqrtf(5)) / 10	} = P1
// { -sqrtf((5 - sqrtf(5)) / 10), (sqrtf(5)) / 5, -(5 + sqrtf(5)) / 10	} = P2
// { -sqrtf((5 + sqrtf(5)) / 10), (sqrtf(5)) / 5, (5 - sqrtf(5)) / 10	} = P3
// {		0,		(sqrtf(5)) / 5,  2 x ((sqrtf(5)) / 5)	} = P4
// {  sqrtf((5 + sqrtf(5)) / 10), (sqrtf(5)) / 5, (5 - sqrtf(5)) / 10	} = P5
// {  sqrtf((5 - sqrtf(5)) / 10), -(sqrtf(5)) / 5,  (5 + sqrtf(5)) / 10	} = P6
// { -sqrtf((5 - sqrtf(5)) / 10), -(sqrtf(5)) / 5, (5 + sqrtf(5)) / 10	} = P7
// { -sqrtf((5 + sqrtf(5)) / 10), -(sqrtf(5)) / 5, -(5 - sqrtf(5)) / 10	} = P8
// {		0,		-(sqrtf(5)) / 5, -2 x ((sqrtf(5)) / 5)	} = P9
// {  sqrtf((5 + sqrtf(5)) / 10), -(sqrtf(5)) / 5, -(5 + sqrtf(5)) / 10	} = P10
// {		0,			-1,		 0	} = P11
//
// Faces :
//
// F0  = P0,  P1,  P2
// F1  = P0,  P2,  P3
// F2  = P0,  P3,  P4
// F3  = P0,  P4,  P5
// F4  = P0,  P5,  P1
// F5  = P2,  P1,  P9
// F6  = P8,  P2,  P9
// F7  = P3,  P2,  P8
// F8  = P7,  P3,  P8
// F9  = P4,  P3,  P7
// F10 = P6,  P4,  P7
// F11 = P5,  P4,  P6
// F12 = P10, P5,  P6
// F13 = P10, P1,  P5
// F14 = P9,  P1,  P10
// F15 = P11, P8,  P9
// F16 = P11, P7,  P8
// F17 = P11, P6,  P7
// F18 = P11, P10, P6
// F19 = P11, P9,  P10

Triangle	*isosahedron (float size)
{
  Dot		*vertex[12];
  Triangle	*face[20];
  float	expr_1 = size * sqrtf((5 + sqrtf(5)) / 10);
  float	expr_2 = size * sqrtf((5 - sqrtf(5)) / 10);
  float	expr_3 = size * (5 + sqrtf(5)) / 10;
  float	expr_4 = size * (5 - sqrtf(5)) / 10;
  float	expr_5 = size * (sqrtf(5)) / 5;

  // Creation of the vertex
  vertex[0] =	new Dot ( 0,		 1,		 0);
  vertex[1] =	new Dot ( expr_2,	 expr_5,	-expr_3);
  vertex[2] =	new Dot (-expr_2,	 expr_5,	-expr_3);
  vertex[3] =	new Dot (-expr_1,	 expr_5,	 expr_4);
  vertex[4] =	new Dot ( 0,		 expr_5,	 2 * expr_5);
  vertex[5] =	new Dot ( expr_1,	 expr_5,	 expr_4);
  vertex[6] =	new Dot ( expr_2,	-expr_5,	 expr_3);
  vertex[7] =	new Dot (-expr_2,	-expr_5,	 expr_3);
  vertex[8] =	new Dot (-expr_1,	-expr_5,	-expr_4);
  vertex[9] =	new Dot ( 0,		-expr_5,	-2 * expr_5);
  vertex[10] =	new Dot ( expr_1,	-expr_5,	-expr_4);
  vertex[11] =	new Dot ( 0,		-1,		 0);

  // Creation of the triangles
  face[0] = new Triangle (vertex[0],	vertex[1],	vertex[2], NULL);
  face[1] = new Triangle (vertex[0],	vertex[2],	vertex[3], NULL);
  face[2] = new Triangle (vertex[0],	vertex[3],	vertex[4], NULL);
  face[3] = new Triangle (vertex[0],	vertex[4],	vertex[5], NULL);
  face[4] = new Triangle (vertex[0],	vertex[5],	vertex[1], NULL);
  face[5] = new Triangle (vertex[2],	vertex[1],	vertex[9], NULL);
  face[6] = new Triangle (vertex[8],	vertex[2],	vertex[9], NULL);
  face[7] = new Triangle (vertex[3],	vertex[2],	vertex[8], NULL);
  face[8] = new Triangle (vertex[7],	vertex[3],	vertex[8], NULL);
  face[9] = new Triangle (vertex[4],	vertex[3],	vertex[7], NULL);
  face[10] = new Triangle (vertex[6],	vertex[4],	vertex[7], NULL);
  face[11] = new Triangle (vertex[5],	vertex[4],	vertex[6], NULL);
  face[12] = new Triangle (vertex[10],	vertex[5],	vertex[6], NULL);
  face[13] = new Triangle (vertex[10],	vertex[1],	vertex[5], NULL);
  face[14] = new Triangle (vertex[9],	vertex[1],	vertex[10], NULL);
  face[15] = new Triangle (vertex[11],	vertex[8],	vertex[9], NULL);
  face[16] = new Triangle (vertex[11],	vertex[7],	vertex[8], NULL);
  face[17] = new Triangle (vertex[11],	vertex[6],	vertex[7], NULL);
  face[18] = new Triangle (vertex[11],	vertex[10],	vertex[6], NULL);
  face[19] = new Triangle (vertex[11],	vertex[9],	vertex[10], NULL);

  // Setting up of the connexity
  face[0]->Make_connexity (face[1]);
  face[0]->Make_connexity (face[4]);
  face[0]->Make_connexity (face[5]);

  face[1]->Make_connexity (face[0]);
  face[1]->Make_connexity (face[2]);
  face[1]->Make_connexity (face[7]);

  face[2]->Make_connexity (face[1]);
  face[2]->Make_connexity (face[3]);
  face[2]->Make_connexity (face[9]);

  face[3]->Make_connexity (face[2]);
  face[3]->Make_connexity (face[4]);
  face[3]->Make_connexity (face[11]);

  face[4]->Make_connexity (face[0]);
  face[4]->Make_connexity (face[3]);
  face[4]->Make_connexity (face[13]);

  face[5]->Make_connexity (face[0]);
  face[5]->Make_connexity (face[6]);
  face[5]->Make_connexity (face[14]);

  face[6]->Make_connexity (face[5]);
  face[6]->Make_connexity (face[7]);
  face[6]->Make_connexity (face[15]);

  face[7]->Make_connexity (face[1]);
  face[7]->Make_connexity (face[6]);
  face[7]->Make_connexity (face[8]);

  face[8]->Make_connexity (face[7]);
  face[8]->Make_connexity (face[9]);
  face[8]->Make_connexity (face[16]);

  face[9]->Make_connexity (face[2]);
  face[9]->Make_connexity (face[8]);
  face[9]->Make_connexity (face[10]);

  face[10]->Make_connexity (face[9]);
  face[10]->Make_connexity (face[11]);
  face[10]->Make_connexity (face[17]);

  face[11]->Make_connexity (face[3]);
  face[11]->Make_connexity (face[10]);
  face[11]->Make_connexity (face[12]);

  face[12]->Make_connexity (face[11]);
  face[12]->Make_connexity (face[13]);
  face[12]->Make_connexity (face[18]);

  face[13]->Make_connexity (face[4]);
  face[13]->Make_connexity (face[12]);
  face[13]->Make_connexity (face[14]);

  face[14]->Make_connexity (face[5]);
  face[14]->Make_connexity (face[13]);
  face[14]->Make_connexity (face[19]);

  face[15]->Make_connexity (face[6]);
  face[15]->Make_connexity (face[16]);
  face[15]->Make_connexity (face[19]);

  face[16]->Make_connexity (face[8]);
  face[16]->Make_connexity (face[15]);
  face[16]->Make_connexity (face[17]);

  face[17]->Make_connexity (face[10]);
  face[17]->Make_connexity (face[16]);
  face[17]->Make_connexity (face[18]);

  face[18]->Make_connexity (face[12]);
  face[18]->Make_connexity (face[17]);
  face[18]->Make_connexity (face[19]);

  face[19]->Make_connexity (face[14]);
  face[19]->Make_connexity (face[15]);
  face[19]->Make_connexity (face[18]);

  return face[0];
}

void		split_all (Triangle *triangle)
{
  if (triangle->Is_checked ())
    return;
  triangle->Checked ();

  triangle->Split ();

  if (triangle->Neighbor_ab () != 0)
    split_all (triangle->Neighbor_ab ());
  if (triangle->Neighbor_bc () != 0)
    split_all (triangle->Neighbor_bc ());
  if (triangle->Neighbor_ca () != 0)
    split_all (triangle->Neighbor_ca ());
}
