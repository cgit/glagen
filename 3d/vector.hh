//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// vector.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		VECTOR_HH_
# define	VECTOR_HH_

class		Vector
{
public :

  // Constructors and destructor
  Vector ();
  Vector (const Vector &);
  Vector (const float, const float, const float);
  ~Vector ();

  // Read
  float	X () const;
  float	Y () const;
  float	Z () const;

  float	Norm () const;
  float	Approx_norm () const;

  // Write
  void		Set (const float, const float, const float);
  void		Set_X (const float);
  void		Set_Y (const float);
  void		Set_Z (const float);
  void		Normalize ();
  void		Approx_normalize ();
  void		Rotate (const Vector &, const float);

  // Operators
  Vector	operator + (const class Vector &) const;
  Vector	operator - (const class Vector &) const;
  float	operator * (const class Vector &) const;
  Vector	operator ^ (const class Vector &) const;
  Vector	operator += (const class Vector &);
  Vector	operator -= (const class Vector &);

  Vector	operator * (const float &) const;
  Vector	operator / (const float &) const;
  Vector	operator *= (const float &);
  Vector	operator /= (const float &);

protected :
  float		vect[3];
};

#endif
