//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// dot.hh for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#ifndef		DOT_HH_
# define	DOT_HH_

#include	"data_glagen.hh"
#include	"triangle.hh"

class			Dot
{
public:

  // Constructor and destructor
  Dot (const float, const float, const float);
  ~Dot ();

  // Read
  float			x () const;
  float			y () const;
  float			z () const;

  class Triangle	*Owner (const int) const;
  class Vector		&Normal ();

  property_t		*Property (const int) const;
  bool			Is_computed () const;
  bool			Is_checked () const;

  // Write
  void			Set (const float, const float, const float);
  void			Use (class Triangle *, class Triangle *);
  bool			Drop (class Triangle *, class Triangle *);
  void			Update_normal ();
  void			Del_property (const int);
  void			Set_property (const int, property_t *);
  void			Computed ();
  void			Checked ();

  // Other
  Dot			*Middle (const Dot *);
  void			Display (const unsigned int);

protected:
  void			Change_owner (class Triangle *, class Triangle *);

  float			_x;
  float			_y;
  float			_z;
  unsigned char		_use;
  class Triangle	*_owner[6];
  class Vector		_normal;
  property_t		**_property;
  bool			_computed;
  char			step;
};

#endif		// DOT_HH_
