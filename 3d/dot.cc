//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// dot.cc for Glagen : made by Zavie (Julien Guertault)
//
// www.glagen.org
//
//=============================================================================

#include	<cstdlib>
#include	<cmath>
#include	"data_glagen.hh"
#include	"dot.hh"

// Constructor and destructor
Dot :: Dot (const float x, const float y, const float z):
  _x(x), _y(y), _z(z), _use(0), _computed(false)
{
  _normal = Vector (0, 0, 0);
  _property = new property_t *[glagen.property]; // delete : dot.cc l61
  for (int current = 0; current < glagen.property; current++)
    _property[current] = NULL;
  for (int current = 0; current < 6; current++)
    _owner[current] = NULL;
  step = glagen.step;
}

Dot :: ~Dot ()
{
  if (_property != NULL)
    {
      for (int current = 0; current < glagen.property; current++)
	if (_property[current] != NULL)
	  {
	    if (_property[current]->data != NULL)
	      delete _property[current]->data; // FIXME : warning here.
	    delete _property[current];
	  }
      delete _property;
    }
}


// ======================================================================= Read

float		Dot :: x () const		{ return _x; }
float		Dot :: y () const		{ return _y; }
float		Dot :: z () const		{ return _z; }

Triangle	*Dot :: Owner (int triangle) const
{
  if (triangle >= 0 && triangle < 6)
    return _owner[triangle];
  return NULL;
}

Vector		&Dot :: Normal ()		{ return _normal; }

property_t	*Dot :: Property (const int i) const
{
  return _property[i];
}

bool		Dot :: Is_computed () const
{
  return _computed;
}

bool		Dot :: Is_checked () const
{
  return (step == glagen.step);
}


// ====================================================================== Write

void		Dot :: Set (const float x, const float y, const float z)
{
  _x = x;
  _y = y;
  _z = z;
}

void		Dot :: Use (Triangle *old_one, Triangle *new_one)
{
  Change_owner (old_one, new_one);
  _use = _use + 1;
}

bool		Dot :: Drop (Triangle *old_one, Triangle *new_one)
{
  Change_owner (old_one, new_one);
  _use = _use - 1;
  return (_use <= 0);
}

void		Dot :: Update_normal ()
{
  int		count = 0;

  if (step == glagen.step)
    return;
  step = glagen.step;

  _normal.Set (0, 0, 0);
  for (int current = 0; current < 6; current++)
    if (_owner[current] != NULL)
      {
	_normal = _normal + _owner[current]->Normal_real ();
	count++;
      }
  if (count != 0)
    _normal = _normal / count;
}

void		Dot :: Del_property (const int i)
{
  if (_property[i] != NULL)
    {
      delete _property[i];
      _property[i] = NULL;
    }
}

void		Dot :: Set_property (const int i, property_t *property)
{
  _property[i] = property;
}

void		Dot :: Computed ()		{ _computed = true; }

void		Dot :: Checked ()		{ step = glagen.step; }

// ====================================================================== Other

Dot		*Dot :: Middle (const Dot *b)
{
  float xc = (x () + b->x ()) / 2;
  float yc = (y () + b->y ()) / 2;
  float zc = (z () + b->z ()) / 2;
  float adjust = sqrtf (glagen.square_size / (xc * xc + yc * yc + zc * zc));
  xc = xc * adjust;
  yc = yc * adjust;
  zc = zc * adjust;
  return new Dot (xc, yc, zc); // delete : triangle.cc l99, 101, 103
}

// ================================================================== Protected

void		Dot :: Change_owner (Triangle *old_one, Triangle *new_one)
{
  for (int current = 0; current < 6; current++)
    if (_owner[current] == old_one)
      {
	_owner[current] = new_one;
	return;
      }
  for (int current = 0; current < 6; current++)
    if (_owner[current] == NULL)
      {
	_owner[current] = new_one;
	return;
      }
}
