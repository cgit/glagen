//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// libclass.hh for Glagen : made by Hugues HIEGEL
//
// www.glagen.org
//
//=============================================================================
//--LIBRARY CLASS DEFINITION--//

#include <iostream>
#include <string>
#include <list>

#include "../../3d/data_glagen.hh"
#include "Data_property.hh"

#ifndef LIBCLASS_HH_
# define LIBCLASS_HH_

using namespace std;
extern int GLG_property_pos;

typedef struct dot_property_t {
  int		pos;
  size_t	size;
  kind_lib_t	type;
};

class Library
{
public:

  Library();
  Library(string Filename);
  virtual ~Library();

  //  int	FirstLoad(); // ???
  int				LoadLib();
  int				LoadLib(string Filename);
  int			 	Initialize();
  int			 	MainLoop(Dot* dot);
  void				_palloc(kind_lib_t type, size_t size);

  //  int	MainLoop(Dot* dot, ...);

  int	Uninitialize();
  void	UnloadLib();

  list<string>		getDependancies() const;
  string		getName() const;
  string		getFilename() const;
  list<dot_property_t>	getDotProperties() const;
  int			getRealPos(const int pos) const;
  int			AccesAllowed(string& ToLib) const;

private:
  string			Filename;
  void				*handler;
  std::list<string>		dependancies;
  std::list<dot_property_t>	properties;

};

void palloc(kind_lib_t type, size_t size, Library* REF);

void GLG_write(int pos, size_t size, void* data, Library* REF);
void GLG_read(string& LibName, int pos, size_t size, void* data, Library* REF);
void GLG_read(int pos, size_t size, void* data, Library* REF);

#endif //LIBCLASS_HH_
