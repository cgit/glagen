//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// Data_property.hh for Glagen : made by Titi (Meng-tih LAM)
//
// www.glagen.org
//
//=============================================================================

#ifndef		DATA_PROPERTY_HH_
# define	DATA_PROPERTY_HH_

#include <iostream>
#include <string>
#include <unistd.h> // Pour write/read

#include <errno.h>

#include "../../3d/data_glagen.hh"

class Data_property
{
public:
  Data_property() : _data(0), _size(0), _type(kind_lib_t(none)) {}
  Data_property(void* data, size_t size, kind_lib_t type)
    : _data(data),
      _size(size),
      _type(type)
  {}

  void	write_data(const int& fd) const
  {
    write(fd, &_type, sizeof(int));
    write(fd, &_size, sizeof(int));
    write(fd, _data, _size * sizeof(void *));
  }

  void	read_data(const int& fd)
  {
    do
      {
	errno = 0;
	read(fd, &_type, sizeof(int));
      }
    while (errno == 4);

    do
      {
	errno = 0;
	read(fd, &_size, sizeof(int));
      }
    while (errno == 4);

    do
      {
	errno = 0;
	read(fd, _data, _size * sizeof(void *));
      }
    while (errno == 4);
  }

protected:
  void		*_data;
  size_t	_size;
  kind_lib_t	_type;
};

#endif		// DATA_PROPERTY_HH_
