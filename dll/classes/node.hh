
#ifndef		NODE_HH_
# define	NODE_HH_

using namespace std;

#include <list>
#include <cassert>

// Rajouter l'include correspondant a la template de TYPENODE
// + implemente le std::cout abtrait pour les tests

template<class TYPENODE> class Tree;	// Declaration prealable

template<class TYPENODE> class Node
{
  friend class Tree<TYPENODE>;

public:
  Node(const TYPENODE &d) : _link(new std::list<Node>), _data(d) {}
  
  void	add_child(const TYPENODE& value) { _link->push_back(Node(value)); }

  void	add_tree_node(const Node& value) { _link->push_back(value); }

  TYPENODE get_data() const { return (_data); }

  const unsigned int	get_nb_childs() const { return (_link->size()); }

  std::list<Node>* get_childs() const { return (_link); }

  unsigned int	get_number_node() const
  {
    unsigned int number_childs = _link->size();
    std::list<Node>::const_iterator child = _link->begin();
    unsigned int number_node = 1;
    for (unsigned int tmp = 0; tmp != number_childs; ++tmp)
      {
  	number_node += child->get_number_node();
  	++child;
      }
    return (number_node);
  }

  // Renvoie le noeud fils suivant le numero a partir de 0
  Node	get_child(const unsigned int& i) const
  {
    assert(i < _link->size());
    std::list<Node>::const_iterator child = _link->begin();
    for (unsigned int tmp = 0; tmp != i; ++tmp)
      ++child;
    return (*child);
  }

  // Numerotation de l'arbre suivant un parcours en largeur de 0 a n
  Node	get_node_course_width(const int& ptr) const
  {
    int rptr = ptr;
    std::list<Node> ptr_node = *new std::list<Node>;
    ptr_node.push_back(*this);
    while (ptr_node.size())
      {
	std::list<Node>::iterator childs = ptr_node.begin();
	std::list<Node>::iterator child = childs->get_childs()->begin();
	for (; child != childs->get_childs()->end(); ++child)
	  {
	    ptr_node.push_back(*child);
	    if (--rptr == 0)
	      return (*child);
	  }
	ptr_node.pop_front();
      }
    return (*this);
  }

private:
  
  std::list<Node> *_link;	// Pointeur vers le sous-arbre
  TYPENODE _data;
};

#endif	// NODE_HH_
