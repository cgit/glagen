//
// dot.hh for Glagen in ~/Galgen/3d
// 
// Made by Zavie
// Login   <guerta_j@epita.fr>
// 
// Started on  Fri Aug 16 17:08:16 2002 Zavie
//

#ifndef		DOT_HH_
# define	DOT_HH_

class		Dot
{
public:

  // Constructor and destructor
  Dot (double x, double y, double z);
  ~Dot ();

  // Reading
  double	x ();
  double	y ();
  double	z ();

  void		*Property (int i);
  bool		Is_checked ();

  // Writing
  void		set (double x, double y, double z);
  void		Use ();
  void		Drop ();
  void		Del_property (int i);
  void		Set_property (int i, void *property);
  void		Checked ();

  // Other tools
  Dot		*Middle (Dot *b);

protected:
  double	_x;
  double	_y;
  double	_z;
  unsigned char	_use;
  void		**_property;
  char		step;
};

#endif		// DOT_HH_
