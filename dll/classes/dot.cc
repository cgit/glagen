//
// dot.cc for Glagen in ~/Galgen/3d
// 
// Made by Zavie
// Login   <guerta_j@epita.fr>
// 
// Started on  Fri Aug 16 17:08:16 2002 Zavie
//

#include	<cstdio>
#include	<cstdlib>
#include	<cmath>
#include	"data_glagen.hh"
#include	"dot.hh"

// Constructor and destructor
Dot :: Dot (double x, double y, double z)
{
  int	current;

  _x = x;
  _y = y;
  _z = z;
  _use = 0;
  _property = new void *[GL_property];
  for (current = 0; current < GL_property; current++)
    _property[current] = NULL;
  step = GL_step;
}

Dot :: ~Dot ()
{
  int	current;

  for (current = 0; current < GL_property; current++)
    if (_property[current] != NULL)
      delete _property[current];
}

// Reading
double	Dot :: x ()		{ return _x; }
double	Dot :: y ()		{ return _y; }
double	Dot :: z ()		{ return _z; }

void	*Dot :: Property (int i)
{
  return _property[i];
}
bool	Dot :: Is_checked ()	{ return (step == GL_step); }

// Writing
void	Dot :: set (double x, double y, double z)
{
  _x = x;
  _y = y;
  _z = z;
}

void	Dot :: Use ()		{ _use = _use + 1; }

void	Dot :: Drop ()
{
  _use = _use - 1;
  if (0 == _use)
    {
      delete this;
      printf("destroy !\n");
    }
}

void	Dot :: Del_property (int i)
{
  delete _property[i];
  _property[i] = NULL;
}

void	Dot :: Set_property (int i, void *property)
{
  _property[i] = property;
}

void	Dot :: Checked ()	{ step = GL_step; }

// Other tools
Dot	*Dot :: Middle (Dot *b)
{
  double	xc;
  double	yc;
  double	zc;
  double	adjust;

  xc = (this->x () + b->x ()) / 2;
  yc = (this->y () + b->y ()) / 2;
  zc = (this->z () + b->z ()) / 2;
  adjust = sqrt (GL_square_size / (xc * xc + yc * yc + zc * zc));
  xc = xc * adjust;
  yc = yc * adjust;
  zc = zc * adjust;
  return new Dot (xc, yc, zc);
}
