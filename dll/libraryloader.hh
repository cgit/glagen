//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// maintest.hh for Glagen : made by Hugues HIEGEL
//
// www.glagen.org
//
//=============================================================================

#ifndef LIBRARYLOADER_CC_
# define LIBRARYLOADER_CC_

#include "classes/libclass.hh"
#include "classes/node.hh"
#include "classes/Data_string.hh"

extern int GLG_property_pos;

Node<Data_string> LibraryLoader(const list<string>& LibFiles);

#endif //LIBRARYLOADER_CC_
