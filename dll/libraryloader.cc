//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// libraryloader.cc for Glagen : made by Hugues HIEGEL
//
// www.glagen.org
//
//=============================================================================

#include <iostream>
#include <string>
#include <list>

#include "includes/errors.hh"
#include "classes/libclass.hh"
#include "libraryloader.hh"

#include "classes/node.hh"
#include "classes/matrix.hh"
#include "classes/Data_string.hh"


using std::cout;
using std::cerr;
using std::endl;

int
getLibID(const std::list<Library>* libids,
	 const string& name)
{
  // Searchs for an ID of the given library name
  
  if (libids->empty())
    return (-1);
  
  int libid = 0;
  std::list<Library>::const_iterator libids_i = NULL;
  
  for (libids_i = libids->begin();
       (libids_i != libids->end()) && ((libids_i->getName()) != name);
       libids_i++)
    libid++;
  
  if (libid == (signed)libids->size())
    return (-1);

  return libid;

}


void
RecursiveAddNode(const Node<int>* Root,
		 Matrix<int>* Adj,
		 const int size)
{
  int curr_node = 0;
  const int nb_childs = Root->get_nb_childs();
  
  if (nb_childs)
    {
      for (curr_node = 0; curr_node < nb_childs; curr_node++)
	{
	  Node<int> node = Root->get_child(curr_node);
	  
	  // Which library is a potentially dependancy ?
	  int curr_lib = node.get_data();
	  
	  // Which libraries need it ?
	  for (int i_ = 0; i_ < size; i_++)
	    if ((*Adj)(i_, curr_lib) == 1)
	      {
		node.add_child(i_);
		(*Adj)(i_, curr_lib) = 2;
	      }
	  RecursiveAddNode(&node, Adj, size);
	}
    }
 
  return;
     
}

void
Int2Datanode(Node<int>* Root,
	     Node<Data_string>* Final,
	     list<Library>* list_libs)
{
  int curr_node = 0;
  const int nb_childs = Root->get_nb_childs();
  std::list<Library>::const_iterator libid_i;
  int i;
  int I;
  
  if (nb_childs)
    {
      for (curr_node = 0; curr_node < nb_childs; curr_node++)
	{
	  Node<int> node = Root->get_child(curr_node);
	  I = node.get_data();
	  
	  libid_i = list_libs->begin();
	  for (i = 0; i < I; i++)
	    libid_i++;

	  Final->add_child(Data_string(libid_i->getFilename()));
	  Node<Data_string> TotoMesCouilles = 
	    Final->get_child(Final->get_nb_childs() - 1);
	  Int2Datanode(&node, &TotoMesCouilles, list_libs); 
	}
    }
 
  return;
     
}

// 
// #
// #         ####     ##    #####   ######  #####
// #        #    #   #  #   #    #  #       #    #
// #        #    #  #    #  #    #  #####   #    #
// #        #    #  ######  #    #  #       #####    ###     ###
// #        #    #  #    #  #    #  #       #   #    ###     ###
// #######   ####   #    #  #####   ######  #    #   ###     ###
// 


Node<Data_string>
LibraryLoader(const list<string>& LibFiles)
{

  /**************************************************************/
  /* Opens all selected libraries and sort them by dependancies */
  /**************************************************************/

  /*\****************\* Declarations *\***************\*/

  /* Pointer needed to manipulate libraries */
  Library*	current_loading (NULL);
  string	libname;

  /* List of loaded libraries needed to construct the deps tree */
  std::list<Library> libids;
  libids.clear();
  int	libid;

  /***********************************/
  /* Loads libraries and stores them */
  /***********************************/
  for (std::list<string>::const_iterator libfile_i = LibFiles.begin();
       libfile_i != LibFiles.end();
       libfile_i++)
    {
      current_loading = new Library (*libfile_i);  // Creates new class
      
      if (! current_loading->LoadLib())
	{

	  /**********************************************
	  ** just for test purposes		       **
	  ** it has nothing to do there		       **
	  ** if it isn't commented.. comment it !      **
	  **********************************************/
	  //current_loading->Initialize();
apao
	  if (getLibID(&libids, current_loading->getName()) == -1)
	    libids.push_back(*current_loading);	// Stores the lib
	  else
	    {
	      cerr << "[31;1mLibrary "
		   << current_loading->getName()
		   << " is already loaded ![0m" << endl;
	      delete current_loading;
	    }
	}
      else
	delete current_loading;

    } 



  /*\****************\* Declarations *\***************\*/

  int size = libids.size();

  /* Matrix of dependancies */
  Matrix<int>	adjacence (size, size);
  int i_ = 0, j_ = 0;

  /* List needed to manipulate dependancies of current lib */
  std::list<string> deps;
  deps.clear();

  /* Lists iterators to iterate lists ;o) */
  std::list<Library>::const_iterator libid_i;
  std::list<string>::const_iterator dep_i;

  /**************************************/
  /* Second parsing to get dependancies */
  /**************************************/

  for (libid_i = libids.begin(); libid_i != libids.end(); libid_i++)
    {
      deps = libid_i->getDependancies();
      
      if (! deps.empty())
	for (dep_i = deps.begin();
	     dep_i != deps.end();
	     dep_i++)
	  {
      	    libid = getLibID(&libids, *dep_i);
	    if (!(libid < 0))
	      adjacence(i_, libid) = 1;
	    else
	      {
		cerr
		  << "[31;1m#ERR " << ERR_LIBMISSING
		  << ": Missing library: "
		  << *dep_i << "[0m" << endl;
		exit(ERR_LIBMISSING);
	      }
	  }
      
      i_++;

    }
  
  cout << "-- " << endl << adjacence << "-- " << endl;

  
  /*\****************\* Declarations *\***************\*/
  /* The root node of sorted libraries tree */
  Node<int> root (-1);
      
  int n_deps;

  /***************************************/
  /* Search for potential root libraries */
  /***************************************/

  for (i_ = 0;
       i_ < size;
       i_++)
    {
      n_deps = 0;
      for (j_ = 0; j_ < size; j_++)
	if (adjacence(i_, j_))
	  n_deps += 1;

      cout << i_ << " : " << n_deps;

      if (n_deps == 0)		/* This is a root library */
	{
	  root.add_child(i_);
	  cout << " [32;1mAdded in Root tree !";
	}
      cout << "[0m" << endl;
    }

  cout << endl << "Root has " << root.get_nb_childs() << " childs.."
       << endl << endl;
  
  if (!root.get_nb_childs())	/* No independant library found.. */
    {
      cerr << "[31;1m#ERR " << ERR_NOROOTLIB
	   << ": No independant library found..[0m" << endl;
      exit(ERR_NOROOTLIB);
    }

  /***********************************/
  /* Recursive addition of each node */
  /***********************************/

  RecursiveAddNode(&root, &adjacence, size);

  cout << "-- " << endl << adjacence << "-- " << endl;
  
  /******************************************************/
  /* Checks if every Library has been added in the tree */
  /******************************************************/

  for (i_ = 0;
       i_ < size;
       i_++)
    for (j_ = 0;
	 j_ < size;
	 j_++)
      if (adjacence(i_, j_) == 1)
	{
	  cerr << "[31;1m#Err " << ERR_NOROOTLIB
	       << ": Infinite loop in library dependancies[0m;" << endl;
	  exit(ERR_NOROOTLIB);
	}

  Node<Data_string> FinalResult (Data_string(""));
  Int2Datanode(&root, &FinalResult, &libids);

  // WHEEEE !! IT'S THE END !!! I CAN'T BELIEVE IT !!
  return FinalResult;
}

