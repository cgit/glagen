/*
**
**	GLAGEN Project Errors values
**		2002 aug 25
**
**	    (made by hiegel_h)
*/

#define ERR_ARGNUM	1
#define ERR_OPENFILE	2

#define ERR_NOLIBARG	3
#define ERR_LIBMISSING	4
#define ERR_OPENLIB	2
#define ERR_LIBSYMBOL	10
#define ERR_LIBNAME	11

#define ERR_NOROOTLIB   21

