//=============================================================================
//
// Glagen : a planet sized landscape generator
// Copyright (C) 2002  Julien Guertault, Hugues Hiegel, Meng-Tih Lam
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//=============================================================================
//
// Glagen : GPL LAndscape GENerator
//
// create_dotproperty_init.cc for Glagen : speedy made by Hugues HIEGEL
//
// www.glagen.org
//
//=============================================================================

#include <list>
#include "classes/libclass.hh"
#include "../3d/data_glagen.hh"

/*****************************************************************
**    Surement la plus crade de mes creations.                  **
**    La faute a ma comprehension qui a l'acuite de la          **
**    bave d'escargot sur le sable Saharien.                    **
*****************************************************************/

list<property_t> Client_DotPropertyTab;
int GLG_property_pos = 0;

list<property_t> Create_DotProperty_Init(list<Library> ClientLibraries)
{
  std::list<Library>::iterator			lib_i;
  std::list<property_t>				PropertyList;
  std::list<dot_property_t>			PropLibList;
  std::list<dot_property_t>::const_iterator	libprop_i;
  property_t					prop;


  /*************************************
  **  We open libraries one by one    **
  **  according the list order        **
  *************************************/

  for (lib_i = ClientLibraries.begin();
       lib_i != ClientLibraries.end();
       lib_i++)
    {
      lib_i->Initialize();

        /**************************************
	**  Then we read the properties one  **
	**  by one according the library..   **
	**************************************/

      PropLibList = lib_i->getDotProperties();
      for (libprop_i = PropLibList.begin();
	   libprop_i != PropLibList.end();
	   libprop_i++)
	{

	  prop.kind = libprop_i->type;
	  prop.size = libprop_i->size;
	  prop.data = malloc(libprop_i->size);

	  PropertyList.push_back(prop);

	}
      
    }

  return PropertyList;
  
}
