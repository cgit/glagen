#include <list>
#include <string>
#include "libraryloader.hh"


extern int GLG_property_pos;
list<string> Libs;

int
main(int argc,
     char **argv)
{

  for (int i = 1; i < argc; i++)
    Libs.push_back(argv[i]);

  GLG_property_pos = 0;

  LibraryLoader(Libs);
  return 0;
}
